# -*- coding: utf-8 -*-
import configparser
import psycopg2 as pg
import pandas as pd
import webbrowser
import matplotlib.pyplot as plt
import matplotlib.colors as clrs
import matplotlib.patches as mpatches
from matplotlib import pyplot as plt, patches, rcParams, cm
import numpy as np
from dateutil.relativedelta import relativedelta
import datetime
from matplotlib.backend_tools import ToolBase
import matplotlib
import os
import sys
from matplotlib.widgets  import RectangleSelector

from matplotlib.widgets import RadioButtons
from matplotlib.widgets import CheckButtons
import matplotlib.lines as mlines

import PyQt5
matplotlib.use('qt5agg')

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QWidget, QAction, QTabWidget,QVBoxLayout, QMessageBox
import qgis.utils
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
import PyQt5
import qgis.utils

from qgis.core import QgsProject, QgsVectorLayer, QgsVectorFileWriter, QgsFeatureRequest, QgsExpression

QgsProject.instance().mapLayers().values()
matplotlib.rcParams["toolbar"] = "toolmanager"

#qgs = QgsApplication([], False)
#qgs.initQgis()



__author__ = 'Simon Makwarth <simak@mst.dk>'
__maintainer__ = 'Simon Makwarth <simak@mst.dk>'
__licence__ = 'GNU GPL v3'


class Database:
    """Class handles all database related function"""
    def __init__(self, database):
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/reader/reader.ini'
        self.database = database
        self.usr_read, self.pw_read, self.host, self.port, self.database = self.parse_db_credentials()

    def parse_db_credentials(self):
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.database}READER']['userid']
        pw_read = config[f'{self.database}READER']['password']
        host = config[f'{self.database}READER']['host']
        port = config[f'{self.database}READER']['port']
        dbname = config[f'{self.database}READER']['databasename']

        return usr_read, pw_read, host, port, dbname

    def connect_to_pg_db(self):
        try:
            pg_con = pg.connect(
                host=self.host,
                port=self.port,
                database=self.database,
                user=self.usr_read,
                password=self.pw_read
            )
        except Exception as e:
            print(e)
            raise ConnectionError('Could not connect to database')
        else:
            print('Connected to database: ' + self.database)

        return pg_con

    def sql_to_df(self, sql):
        try:
            with self.connect_to_pg_db() as con:
                df = pd.read_sql(sql=sql, con=con)
        except Exception as e:
            print(e)
            df = None

        return df
    
class plotWindow():
    def __init__(self, parent=None):
        self.app = QApplication(sys.argv)
        self.MainWindow = QMainWindow()
        self.MainWindow.__init__()
        self.MainWindow.setWindowTitle("plot window")
        self.canvases = []
        self.figure_handles = []
        self.toolbar_handles = []
        self.tab_handles = []
        self.current_window = -1
        self.tabs = QTabWidget()
        self.MainWindow.setCentralWidget(self.tabs)
        self.MainWindow.resize(1280, 900)
        self.MainWindow.show()

    def addPlot(self, title, figure):
        new_tab = QWidget()
        layout = QVBoxLayout()
        new_tab.setLayout(layout)

        figure.subplots_adjust(left=0.05, right=0.99, bottom=0.05, top=0.91, wspace=0.2, hspace=0.2)
        new_canvas = FigureCanvas(figure)
        new_toolbar = NavigationToolbar(new_canvas, new_tab)

        layout.addWidget(new_canvas)
        layout.addWidget(new_toolbar)
        self.tabs.addTab(new_tab, title)

        self.toolbar_handles.append(new_toolbar)
        self.canvases.append(new_canvas)
        self.figure_handles.append(figure)
        self.tab_handles.append(new_tab)

    def show(self):
        self.app.exec_()

class Geophysics:
    """class handles the plotting of geophysical data from pcgerda"""
    def __init__(self, x_min_val, y_min_val, x_max_val, y_max_val, data_type_val):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.x_min = x_min_val
        self.y_min = y_min_val
        self.x_max = x_max_val
        self.y_max = y_max_val
        self.data_type = data_type_val

    def fetch_geophysics(self, srid=25832):
        if self.data_type == 'ttem':
            where_clause = f''' AND lower(oms."datatype") = 'tem' AND lower(oms.datsubtype) = 'ttem' '''
        elif self.data_type == 'tem':
            where_clause = f''' AND lower(oms."datatype") = 'tem' 
                AND COALESCE(lower(oms.datsubtype), 'test') NOT IN ('skytem1', 'skytem2', 'skytemmin1', 'ttem')'''
        elif self.data_type == 'skytem':
            where_clause = f''' AND lower(oms."datatype") = 'tem' 
                                AND lower(oms.datsubtype) IN ('skytem1', 'skytem2', 'skytemmin1')'''
        else:
            where_clause = f''' AND oms."datatype" = '{self.data_type}' '''

        sql_models = f'''
                   WITH
                        models AS 
                            (
                                 SELECT  
                                    '{self.data_type}' AS datatype_model,
                                    m.project,
                                    ol.model, 
                                    ol."position" as moposition, 
                                    ol.layer, 
                                    ol.rho, 
                                    ol.rho/NULLIF(ol.rhostddev, 0) AS rho_min, 
                                    ol.rho*NULLIF(ol.rhostddev, 0) AS rho_max, 
                                    COALESCE(ol.depbottom - ol.thickness, LAG(depbottom, 1) OVER 
                                        (PARTITION BY ol.model, ol."position" ORDER BY ol.layer)) AS layer_top,
                                    COALESCE(ol.depbottom, LAG(depbottom + ol.thickness, 1) OVER 
                                        (PARTITION BY ol.model, ol."position" ORDER BY ol.layer)) AS layer_bot,
                                    od.doiupper,
                                    od.doilower,
                                    oms."datatype",
                                    oms.datsubtype,
                                    ol.dbotstddev
                                FROM mstgerda.msttb_model_geometry geo 
                                INNER JOIN gerda.model m USING (model)
                                INNER JOIN gerda.odvlayer ol ON geo.model = ol.model AND geo."position" = ol."position"
                                LEFT JOIN gerda.odvdoi od ON geo.model = od.model AND geo."position" = od."position" 
                                LEFT JOIN gerda.odvmodse oms ON geo.model = oms.model
                                WHERE geo.geom && 
                                    ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
                                    {where_clause}
                            ),
                        rho AS 
                            (
                                SELECT 
                                    project,
                                    model, 
                                    moposition,
                                    layer,
                                    rho,
                                    doiupper,
                                    doilower
                                FROM models
                            ),
                        top_bot AS 
                            (
                                SELECT 
                                    project, model, moposition, 
                                    layer, layer_top AS layer_mut
                                FROM models
                                UNION ALL 
                                SELECT 
                                    project, model, moposition, 
                                    layer, layer_bot
                                FROM models
                            )
                    SELECT    
                        *
                    FROM top_bot
                    INNER JOIN rho USING (project, model, moposition, layer)
                    ORDER BY layer, layer_mut
                    ;
                '''

        sql_data = f'''
            SELECT 
                '{self.data_type}' AS datatype_model,
                ofr.model, 
                ofr.moposition,
                ofr.dataset, 
                ofr.daposition, 
                ofr.segment, 
                ofr."sequence", 
                ofr.abscival, 
                ofr.ordiresval, 
                ofr.ordimeaval, 
                (ordimeaval * ordistdval) - (ordimeaval / ordistdval) AS error                    
            FROM mstgerda.msttb_model_geometry geo
            INNER JOIN gerda.odvfwres ofr ON geo.model = ofr.model AND geo."position" = ofr.moposition
            INNER JOIN gerda.odvmodse oms USING (dataset)
            WHERE geo.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
                {where_clause} ORDER BY dataset, model, moposition, daposition, segment, abscival
            ;
        '''

        print(sql_models)
        print(sql_data)

        db = Database(database='GERDA')
        df_data = db.sql_to_df(sql_data)
        df_models = db.sql_to_df(sql_models)

        return df_data, df_models

    def plot_geophysics(self):

        df_data, df_models = self.fetch_geophysics()
        df_data_grp = df_data.groupby(['dataset', 'model', 'moposition', 'daposition', 'segment'])
        df_model_grp = df_models.groupby(['project', 'model', 'moposition'])

        fig, axs = plt.subplots(1, 2)
        ax1_data = axs[0]
        ax2_model = axs[1]

        fig.suptitle(f'Datatype:{self.data_type}', fontweight='bold')

        # plot data sounding
        for name, grp in df_data_grp:
            ax1_data.errorbar(grp['abscival'], grp['ordimeaval'], grp['error'], fmt='.', color='black', ecolor='red')
            ax1_data.plot(grp['abscival'], grp['ordiresval'], color='blue')

        ax1_data.set_title("Data")
        ax1_data.set_yscale('log')
        ax1_data.set_xscale('log')
        ax1_data.set(xlabel='Time [s] or focusdepth [m]', ylabel='dB/dt or rhoa')
        ax1_data.grid(b=None, which='major', axis='both')
        ax1_black_patch = mpatches.Patch(color='black', label='Data')
        ax1_red_patch = mpatches.Patch(color='red', label='Error')
        ax1_blue_patch = mpatches.Patch(color='blue', label='Fit')
        ax1_handle = [ax1_black_patch, ax1_red_patch, ax1_blue_patch]
        ax1_data.legend(handles=ax1_handle, loc='lower left', fontsize='xx-small')

        # plot geophysical model
        for name, grp in df_model_grp:
            row_count = grp.shape[0] / 2  # number of model layers
            if row_count < 10:
                ax2_model.plot(grp['rho'], grp['layer_mut'], color='red')
            if row_count > 10:
                ax2_model.plot(grp['rho'], grp['layer_mut'], color='blue')

            # plot doi on model plot
            doiextent_max = grp['rho'].max()
            doiextent_min = grp['rho'].min()
            doi_upper = grp['doiupper'].unique()
            doi_lower = grp['doilower'].unique()
            try:
                ax2_model.plot([doiextent_min, doiextent_max], [doi_lower, doi_lower], '--', color='black')
                ax2_model.plot([doiextent_min, doiextent_max], [doi_upper, doi_upper], '--', color='black')
            except:
                'Print NO DOI'

        ax2_model.set_title("Model")
        ax2_model.set_yscale('linear')
        ax2_model.set_xscale('linear')
        ax2_model.invert_yaxis()
        ax2_model.set(xlabel='Resistivity [Ω]', ylabel='Depth [m]')
        ax2_model.grid(b=None, which='major', axis='both')
        ax2_black_patch = mpatches.Patch(color='black', label='DOI')
        ax2_red_patch = mpatches.Patch(color='red', label='FLM')
        ax2_blue_patch = mpatches.Patch(color='blue', label='MLM')
        ax2_handle = [ax2_red_patch, ax2_blue_patch, ax2_black_patch]
        ax2_model.legend(handles=ax2_handle, loc='lower right', fontsize='xx-small')

        fig.tight_layout()
        plt.show()


class Waterlevel:
    """class handles the plotting of waterlevel data from pcjupiterxl"""
    def __init__(self, x_min_val, y_min_val, x_max_val, y_max_val, data_type_val):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.x_min = x_min_val
        self.y_min = y_min_val
        self.x_max = x_max_val
        self.y_max = y_max_val
        self.data_type = data_type_val

    def fetch_waterlevel(self, srid=25832):
        sql_wl = f'''
            SELECT 
                wl.guid_intake, 
                concat(REPLACE(wl.boreholeno, ' ', ''), '_', wl.intakeno) AS dgu_intake, 
                wl.timeofmeas AS pejledato,
                b.purpose,
                b.use,
                EXTRACT(YEAR FROM wl.timeofmeas) as Year,
                wl.watlevmsl AS vandstkote,
                b.XUTM32EUREF89,
                b.YUTM32EUREF89
            FROM jupiter.watlevel wl
            INNER JOIN jupiter.borehole b USING (boreholeid)
            WHERE wl.watlevmsl IS NOT NULL
                AND wl.watlevmsl < 500
                AND wl.watlevmsl > -500
                AND b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ORDER BY timeofmeas 
            ;
        '''

        print(sql_wl)

        db = Database(database='JUPITER')
        df_wl = db.sql_to_df(sql_wl)

        return df_wl

    def fetch_waterlevel_gode(self, srid=25832):
        sql_wl_god = f'''
            SELECT 
                wl.guid_intake, 
                concat(REPLACE(wl.boreholeno, ' ', ''), '_', wl.intakeno) AS dgu_intake, 
                wl.timeofmeas AS pejledato,
                b.purpose,
                b.use,
                EXTRACT(YEAR FROM wl.timeofmeas) as Year,
                wl.watlevmsl AS vandstkote,
                b.XUTM32EUREF89,
                b.YUTM32EUREF89
            FROM jupiter.watlevel wl
            INNER JOIN jupiter.borehole b USING (boreholeid)
            INNER JOIN jupiter.screen s using (boreholeid)
            WHERE wl.watlevmsl IS NOT NULL
                AND wl.watlevmsl < 500
                AND wl.watlevmsl > -500
                AND s.top IS NOT NULL
                AND s.bottom IS NOT NULL
                AND b.drilldepth > 10
                AND EXTRACT(YEAR FROM wl.timeofmeas) >= 2000
                AND b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ORDER BY timeofmeas 
            ;
        '''

        db = Database(database='JUPITER')
        god_pejlinger = db.sql_to_df(sql_wl_god)

        return god_pejlinger


    def plot_waterlevel(self):
        df_wl = self.fetch_waterlevel()
        god_pejlinger = self.fetch_waterlevel_gode()
        from matplotlib import pyplot as plt, patches, rcParams, cm
        import numpy as np
        print(df_wl)
        fig, ax = plt.subplots(figsize=[14,8])

        df_wl_grp = df_wl.groupby(['guid_intake', 'dgu_intake'])

        n = sum(df_wl_grp['dgu_intake'].nunique())
        print(n)
        print(type(n))


        color = iter(cm.rainbow(np.linspace(0, 1, n)))
        k = []
        for name, grp in df_wl_grp:
            c = next(color)
            datem = max(grp['pejledato']).year

            if len(grp) >= 3 and datem >= 1980:
                    line, = ax.plot(grp['pejledato'], grp['vandstkote'], '--o', label=name[1], c=c)
                    k.append(line)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        if len(k)<30:
            leg = ax.legend(title='DGU Indtag', fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5), fontsize='small')
        if len(k)>30 and len(k)<60:
            leg = ax.legend(title='DGU Indtag', fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5), ncol=2, fontsize='small')
        if len(k)>60 and len(k)<90:
            leg = ax.legend(title='DGU Indtag', fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5), ncol=3, fontsize='small')
        if len(k)>90:
            leg = ax.legend(title='DGU Indtag', fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5), ncol=4, fontsize='small')

        lines = ax.get_lines()
        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()



        ax.set_xlabel('Date')
        ax.set_ylabel('Waterlevel elevation [m]')
        fig.suptitle(f'Tidsserier - Waterlevel', fontweight='bold')
        ax.grid(visible=None, which='both', axis='both')

        fig.canvas.mpl_connect('pick_event', on_pick)

        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        def update_annot(line, annot, ind):
            posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]
            gid = df_wl.loc[(df_wl['pejledato'] == posx) & (df_wl['vandstkote'] == posy), 'dgu_intake'].iloc[0]
            gid2 = df_wl.loc[(df_wl['pejledato'] == posx) & (df_wl['vandstkote'] == posy), 'purpose'].iloc[0]
            gid3 = df_wl.loc[(df_wl['pejledato'] == posx) & (df_wl['vandstkote'] == posy), 'use'].iloc[0]

            # annot.xycoords = line.axes.transData
            annot.xy = (posx, posy)
            text = f'{gid} ({gid2} - {gid3})'
            annot.set_text(text)
            annot.get_bbox_patch().set_alpha(1)

        def hover(event):
            vis = annot.get_visible()
            if event.inaxes in [ax]:
                for line in lines:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot(line, annot, ind['ind'][0])
                        annot.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot.set_visible(False)
                            fig.canvas.draw_idle()

        fig.canvas.mpl_connect('motion_notify_event', hover)

        def onclicker(event):
            for line in lines:
                if line.contains(event)[0]:
                        cont, ind = line.contains(event)
                        annot2 = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                                     bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                                     arrowprops=dict(arrowstyle="->"))
                        update_annot(line, annot2, ind['ind'][0])
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', onclicker)

        def clearAnn( event):
            if event.inaxes is None:  # event.inaxes returns None if you click outside of the plot area, i.e. in the grey part surrounding the axes
                for ann in ax.get_children():
                    if isinstance(ann, matplotlib.text.Annotation):
                        ann.set_visible(False)
                        event.canvas.draw()
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', clearAnn)
        fig_txt = f'Polygon: {self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}'
        print(fig_txt)

        plt.figtext(0.01, 0.01, fig_txt, fontsize=6, weight='bold')

        class NewTool(ToolBase):

            description = 'Waterlevel er pejletidsserier med mere end 3 målinger og hvor seneste måling er senest fra 1980. \nLegenden er interaktiv og du kan "hover" over et punkt og få boringens DGU-nummer, formål og brug.'

        class NewTool2(ToolBase):
            description = 'Indhente alle pejlinger til QGIS som punkter (shapefile).'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                df_wl.to_csv('C:/Temp/pejlinger_qgis.csv', index=False)

                path = 'file:///' + 'C:/Temp/pejlinger_qgis.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % ("latin0",",", "xutm32euref89", "yutm32euref89","epsg:25832")
                #  "?delimiter=%s&crs=epsg:3857&xField=%s&yField=%s" % (",", "lon", "lat")

                mylayer = QgsVectorLayer(path, "Alle pejlinger", "delimitedtext")

                project.addMapLayer(mylayer)

        class NewTool3(ToolBase):
            description = 'Indhente pejletidsserier fra plot til QGIS som punkter (shapefile).'

            def trigger(self, *args, **kwargs):
                appended_data = []

                for name, grp in df_wl_grp:
                    datem = max(grp['pejledato']).year
                    if len(grp) >= 3 and datem >= 1980:
                        print(grp)
                        appended_data.append(grp)

                appended_data = pd.concat(appended_data)

                appended_data.to_csv('C:/Temp/pejletidsserie_qgis.csv', index=False)

                path = 'file:///' + 'C:/Temp/pejletidsserie_qgis.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % ("latin0",",", "xutm32euref89", "yutm32euref89","epsg:25832")
                #  "?delimiter=%s&crs=epsg:3857&xField=%s&yField=%s" % (",", "lon", "lat")

                mylayer = QgsVectorLayer(path, "Pejletidsserier", "delimitedtext")
                project = QgsProject.instance()

                project.addMapLayer(mylayer)

        class NewTool4(ToolBase):
            description = 'Indhente gode pejlinger (Med filtertop og -bund, boringsdybde over 10m og pejling fra 2000 og frem) til QGIS som punkter (shapefile).'

            def trigger(self, *args, **kwargs):

                god_pejlinger.to_csv('C:/Temp/godepejlinger_qgis.csv', index=False)
                path = 'file:///' + 'C:/Temp/godepejlinger_qgis.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                "latin0", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")
                #  "?delimiter=%s&crs=epsg:3857&xField=%s&yField=%s" % (",", "lon", "lat")

                mylayer = QgsVectorLayer(path, "Gode pejlinger", "delimitedtext")
                project = QgsProject.instance()

                project.addMapLayer(mylayer)

        from PyQt5.QtCore import QDate, QDateTime
        class NewTool5(ToolBase):
            description = 'Marker valgte punkter fra QGIS i plottet. (Kør efter "QGIS - Pejletidsserier fra plot")'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()
                try:
                    z = ax.collections
                    z.pop(0)
                    z.remove()
                except:
                    pass

                if len(project.mapLayersByName('Pejletidsserier')) == 0:
                    QMessageBox.warning(None, 'Meddelse', "Pejletidsserier skal først indhentes.")
                    pass
                else:
                    scatterlag = project.mapLayersByName('Pejletidsserier')[0]

                    valgtepunkter_features = scatterlag.selectedFeatures()

                    lyr = project.mapLayersByName('Pejletidsserier')[0]

                    cols = [f.name() for f in lyr.fields()]


                    df2 = pd.DataFrame(valgtepunkter_features, columns=cols)
                    df2['pejledato'] = df2['pejledato'].astype(str)

                    df2["nydato"] = ''
                    for ind, k in df2['pejledato'].iteritems():
                        if len(k) > 5:
                            index = k.find('(')
                            k2= k[index + 1:]
                            k3 = k2[:-1]
                            k4 =k3.replace(', ', '-')
                            k5 = "-".join(k4.split("-")[:3])
                            df2.at[ind, 'nydato'] = k5
                            df2['nydato'] = pd.to_datetime(df2['nydato'])

                    plt.autoscale(False)
                    scat = plt.scatter(df2['nydato'], df2['vandstkote'], s=120, facecolors='none', edgecolors='black')

                    plt.ion()
                    plt.draw()

        import matplotlib.dates as mdates

        def line_select_callback(eclick, erelease):
            x1, y1 = str(mdates.num2date(eclick.xdata, tz=None)).split('.', 1)[0], eclick.ydata
            x2, y2 = str(mdates.num2date(erelease.xdata, tz=None)).split('.', 1)[0], erelease.ydata

            xdata = df_wl['pejledato']
            ydata = df_wl['vandstkote']


            mask = (xdata > min(x1, x2)) & (xdata < max(x1, x2)) & \
                   (ydata > min(y1, y2)) & (ydata < max(y1, y2))
            print(mask)

            listen = []
            for ind, m in enumerate(mask):
                if m == True:
                    pass
                else:
                    listen.append(ind)

            df3 = df_wl.drop(listen)
            markeringer = df3['dgu_intake'].tolist()
            markeringer2 = str(markeringer).replace('[', '').replace(']', '')
            print(markeringer)
            print(markeringer2)

            project = QgsProject.instance()

            scatterlag = project.mapLayersByName('Pejletidsserier')[0]

            expr = QgsExpression("\"dgu_intake\" IN ({})".format(str(markeringer2)))

            ids = QgsFeatureRequest(QgsExpression(expr)).setFlags(QgsFeatureRequest.NoGeometry).setSubsetOfAttributes(
                [])
            selection = scatterlag.getFeatures(ids)  # = [i.id() for i in it]
            scatterlag.selectByIds([s.id() for s in selection])

        def toggle_selector(event):
            print(' Key pressed.')
            if event.key in ['Q', 'q'] and toggle_selector.RS.active:
                print(' RectangleSelector deactivated.')
                toggle_selector.RS.set_active(False)
            if event.key in ['A', 'a'] and not toggle_selector.RS.active:
                print(' RectangleSelector activated.')
                toggle_selector.RS.set_active(True)

        toggle_selector.RS = RectangleSelector(ax, line_select_callback,
                                               drawtype='box', useblit=True,
                                               button=[1], interactive=True)
        fig.canvas.mpl_connect('key_press_event', toggle_selector)

        tm = fig.canvas.manager.toolmanager
        tm.add_tool("Info (hover)", NewTool)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("Info (hover)"), "toolgroup")

        tm.add_tool("QGIS - Alle pejlinger", NewTool2)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Alle pejlinger"), "toolgroup")

        tm.add_tool("QGIS - Gode pejlinger", NewTool4)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Gode pejlinger"), "toolgroup")

        tm.add_tool("QGIS - Pejletidsserier fra plot", NewTool3)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Pejletidsserier fra plot"), "toolgroup")

        tm.add_tool("QGIS - Marker punkter på plot", NewTool5)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Marker punkter på plot"), "toolgroup")

        plt.show()

class Chemsamp:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""

    def __init__(self, x_min_val, y_min_val, x_max_val, y_max_val, data_type_val):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.x_min = x_min_val
        self.y_min = y_min_val
        self.x_max = x_max_val
        self.y_max = y_max_val
        self.data_type = data_type_val

    def fetch_chemsamp(self, srid=25832):
        """

        :param srid:
        :return:
        """
        sql_wl = f'''
            WITH 
                compounds AS 
                    (
                        SELECT c.code::NUMERIC AS compoundno, c.longtext AS compound
                        FROM jupiter.code c 
                        WHERE c.codetype = 221	
                            AND longtext = '{self.data_type}'
                    ),
                chem_unit AS
                    (
                        SELECT c.code::NUMERIC AS unit, c.longtext AS unit_descr
                        FROM jupiter.code c 
                        WHERE c.codetype = 752
                        )
            
            SELECT 
                gcs.guid_intake, 
                gcs.sampledate, 
                CASE
                    WHEN gca.attribute LIKE '<%' 
                        OR gca.attribute LIKE 'A%' 
                        OR gca.attribute LIKE 'o%' 
                        OR gca.attribute LIKE '0%' 
                        THEN 0::NUMERIC
                    WHEN gca.attribute LIKE 'B%' 
                        OR gca.attribute LIKE 'C%' 
                        OR gca.attribute LIKE 'D%' 
                        OR gca.attribute LIKE 'S%' 
                        OR gca.attribute LIKE '!%' 
                        OR gca.attribute LIKE '/%' 
                        OR gca.attribute LIKE '*%' 
                        OR gca.attribute LIKE '>%' 
                        THEN -99::NUMERIC
                    ELSE gca.amount
                    END AS amount_edit,
                u.unit_descr,
                b.purpose,
                b.use,
                b.xutm32euref89,
                b.yutm32euref89,
                concat(REPLACE(gcs.boreholeno, ' ', ''), '_', gcs.intakeno) AS dgu_intake
            FROM jupiter.grwchemanalysis gca
            INNER JOIN jupiter.grwchemsample gcs USING (sampleid) 
            INNER JOIN jupiter.borehole b USING (boreholeid)
            INNER JOIN compounds USING (compoundno)
            LEFT JOIN chem_unit u ON gca.unit = u.unit
            WHERE COALESCE(gca.amount, -1) > 0
                AND b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ORDER BY gcs.guid_intake, gcs.sampledate
            ;
        '''

        db = Database(database='JUPITER')
        df_wl = db.sql_to_df(sql_wl)

        return df_wl

    def plot_chemsamp(self):
        """

        :return:
        """
        from matplotlib import pyplot as plt, patches, rcParams, cm
        import numpy as np
        df_wl = self.fetch_chemsamp()

        fig, ax = plt.subplots(figsize=(10, 8))
        df_wl_grp = df_wl.groupby(['guid_intake', 'dgu_intake', 'unit_descr'])
        pd.set_option('display.max_rows', 500)
        pd.set_option('display.max_columns', 500)
        pd.set_option('display.width', 1000)

        print(df_wl)

        n = sum(df_wl_grp["dgu_intake"].nunique())

        color = iter(cm.rainbow(np.linspace(0, 1, n)))
        k = []
        for name, grp in df_wl_grp:
            c = next(color)
            datem = max(grp['sampledate']).year

            if self.data_type == 'Nitrat' and len(grp) >= 3 and relativedelta(max(grp['sampledate']), min(grp['sampledate'])).years > 3 and \
                    datem >= 2000 and (sum(grp['amount_edit'])/len(grp)) > 1:
                line, = ax.plot(grp['sampledate'], grp['amount_edit'], '--o', label=name[1], c=c)
                plt.ylabel(f'{self.data_type} [{name[2]}]', fontweight='bold', fontsize=12)
                plt.xlabel('Tid [år]', fontweight='bold', fontsize=12)
                k.append(line)
            if self.data_type == 'Sulfat' and len(grp) >= 3 and relativedelta(max(grp['sampledate']), min(grp['sampledate'])).years > 3 and \
                    datem >= 1970 and (sum(grp['amount_edit']) / len(grp)) > 20:
                line, = ax.plot(grp['sampledate'], grp['amount_edit'], '--o', label=name[1], c=c)
                plt.ylabel(f'{self.data_type} [{name[2]}]', fontweight='bold', fontsize=12)
                plt.xlabel('Tid [år]', fontweight='bold', fontsize=12)
                k.append(line)


        plt.title(f'Tidsserie for {self.data_type}', fontweight='bold', fontsize=14)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        lines = ax.get_lines()

        print(len(k))

        if len(k)<30:
            leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5), fontsize='small')
        if len(k)>30 and len(k)<60:
            leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5), ncol=2, fontsize='small')
        if len(k)>60 and len(k)<90:
            leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5), ncol=3, fontsize='small')
        if len(k)>90:
            leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5), ncol=4, fontsize='small')

        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()

        fig.canvas.mpl_connect('pick_event', on_pick)

        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        def update_annot(line, annot, ind):
            posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]
            gid = df_wl.loc[(df_wl['sampledate'] == posx) & (df_wl['amount_edit'] == posy), 'dgu_intake'].iloc[0]
            gid2 = df_wl.loc[(df_wl['sampledate'] == posx) & (df_wl['amount_edit'] == posy), 'purpose'].iloc[0]
            gid3 = df_wl.loc[(df_wl['sampledate'] == posx) & (df_wl['amount_edit'] == posy), 'use'].iloc[0]

            # annot.xycoords = line.axes.transData
            annot.xy = (posx, posy)
            text = f'{gid} ({gid2} - {gid3})'
            annot.set_text(text)
            annot.get_bbox_patch().set_alpha(1)

        def hover(event):
            vis = annot.get_visible()
            if event.inaxes in [ax]:
                for line in lines:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot(line, annot, ind['ind'][0])
                        annot.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot.set_visible(False)
                            fig.canvas.draw_idle()


        fig.canvas.mpl_connect('motion_notify_event', hover)


        def onclicker(event):
            for line in lines:
                if line.contains(event)[0]:
                        cont, ind = line.contains(event)
                        annot2 = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                                     bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                                     arrowprops=dict(arrowstyle="->"))
                        update_annot(line, annot2, ind['ind'][0])
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', onclicker)

        def clearAnn( event):
            if event.inaxes is None:  # event.inaxes returns None if you click outside of the plot area, i.e. in the grey part surrounding the axes
                for ann in ax.get_children():
                    if isinstance(ann, matplotlib.text.Annotation):
                        ann.set_visible(False)
                        event.canvas.draw()
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', clearAnn)

        plt.grid(b=None, which='both', axis='both')

        fig_txt = f'Polygon: {self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}'
        print(fig_txt)

        plt.figtext(0.01, 0.01, fig_txt, fontsize=6, weight='bold')

        datatype = self.data_type

        class NewTool(ToolBase):
            if datatype == 'Nitrat':
                description = 'Tidsserier for nitrat med mere end 3 målinger fordelt på 3 år, seneste måling er taget efter 2000 og gennemsnittet af koncentrationerne er over 1 mg/L. \n\nLegenden er interaktiv og du kan "hover" over et punkt og få boringens DGU-nummer, formål og brug.\n\nVed at indæstte punkter til QGIS (knap), kan du tegne en rektangel på plottet og de vælges herefter i QGIS.'
            if datatype == 'Sulfat':
                description = 'Tidsserier for sulfat med mere end 3 målinger fordelt på 3 år, seneste måling er taget efter 1970 og gennemsnittet af koncentrationerne er over 20 mg/L. \n\nLegenden er interaktiv og du kan "hover" over et punkt og få boringens DGU-nummer, formål og brug.\n\nVed at indæstte punkter til QGIS (knap), kan du tegne en rektangel på plottet og de vælges herefter i QGIS.'

        class NewTool2(ToolBase):
            description = 'Indhente alle punkter fra plottet.'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                appended_data = []


                for name, grp in df_wl_grp:
                    datem = max(grp['sampledate']).year
                    if datatype == 'Nitrat' and len(grp) >= 3 and relativedelta(max(grp['sampledate']), min(grp['sampledate'])).years > 3 and \
                    datem >= 2000 and (sum(grp['amount_edit'])/len(grp)) > 1:
                        appended_data.append(grp)
                    if datatype == 'Sulfat' and len(grp) >= 3 and relativedelta(max(grp['sampledate']), min(
                            grp['sampledate'])).years > 3 and \
                            datem >= 1970 and (sum(grp['amount_edit']) / len(grp)) > 20:
                        appended_data.append(grp)


                appended_data = pd.concat(appended_data)

                appended_data.to_csv('C:/Temp/tidsserie_{}_qgis.csv'.format(datatype), index=False)

                path = 'file:///' + 'C:/Temp/tidsserie_{}_qgis.csv'.format(datatype) + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "latin0", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")
                #  "?delimiter=%s&crs=epsg:3857&xField=%s&yField=%s" % (",", "lon", "lat")
                mylayer = QgsVectorLayer(path, "Tidsserie - {}".format(datatype), "delimitedtext")

                project.addMapLayer(mylayer)

        class NewTool3(ToolBase):
            description = 'Marker valgte punkter fra QGIS i plottet. (Kør efter "QGIS - Punkter fra plot")'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                try:
                    z = ax.collections
                    z.pop(0)
                    z.remove()

                except:
                    print("No such layer..")

                if len(project.mapLayersByName("Tidsserie - {}".format(datatype))) == 0:
                    QMessageBox.warning(None, 'Meddelse', "Tidsserie skal først indhentes.")
                    pass
                else:
                    scatterlag = project.mapLayersByName("Tidsserie - {}".format(datatype))[0]

                    valgtepunkter_features = scatterlag.selectedFeatures()

                    lyr = project.mapLayersByName("Tidsserie - {}".format(datatype))[0]

                    cols = [f.name() for f in lyr.fields()]

                    df2 = pd.DataFrame(valgtepunkter_features, columns=cols)
                    df2['sampledate'] = df2['sampledate'].astype(str)

                    df2["nydato"] = ''
                    for ind, k in df2['sampledate'].iteritems():
                        if len(k) > 5:
                            index = k.find('(')
                            k2 = k[index + 1:]
                            k3 = k2[:-1]
                            k4 = k3.replace(', ', '-')
                            k5 = "-".join(k4.split("-")[:3])
                            df2.at[ind, 'nydato'] = k5
                            df2['nydato'] = pd.to_datetime(df2['nydato'])

                    plt.autoscale(False)
                    scat = plt.scatter(df2['nydato'], df2['amount_edit'], s=120, facecolors='none', edgecolors='black')

                    plt.ion()
                    plt.draw()

        import matplotlib.dates as mdates

        def line_select_callback(eclick, erelease):
            x1, y1 = str(mdates.num2date(eclick.xdata, tz=None)).split('.', 1)[0], eclick.ydata
            x2, y2 = str(mdates.num2date(erelease.xdata, tz=None)).split('.', 1)[0], erelease.ydata
            xdata = df_wl['sampledate']
            ydata = df_wl['amount_edit']

            mask = (xdata > min(x1, x2)) & (xdata < max(x1, x2)) & \
                   (ydata > min(y1, y2)) & (ydata < max(y1, y2))
            print(mask)

            listen = []
            for ind, m in enumerate(mask):
                if m == True:
                    pass
                else:
                    listen.append(ind)

            df3 = df_wl.drop(listen)
            markeringer = df3['dgu_intake'].tolist()
            markeringer2 = str(markeringer).replace('[', '').replace(']', '')
            print(markeringer)
            print(markeringer2)

            project = QgsProject.instance()

            scatterlag = project.mapLayersByName("Tidsserie - {}".format(datatype))[0]

            expr = QgsExpression("\"dgu_intake\" IN ({})".format(str(markeringer2)))

            # it = scatterlag.getFeatures( QgsFeatureRequest( expr ) )
            ids = QgsFeatureRequest(QgsExpression(expr)).setFlags(QgsFeatureRequest.NoGeometry).setSubsetOfAttributes(
                [])
            selection = scatterlag.getFeatures(ids)  # = [i.id() for i in it]
            scatterlag.selectByIds([s.id() for s in selection])
            # scatterlag.setSelectedFeatures(selection)

        def toggle_selector(event):
            print(' Key pressed.')
            if event.key in ['Q', 'q'] and toggle_selector.RS.active:
                print(' RectangleSelector deactivated.')
                toggle_selector.RS.set_active(False)
            if event.key in ['A', 'a'] and not toggle_selector.RS.active:
                print(' RectangleSelector activated.')
                toggle_selector.RS.set_active(True)

        toggle_selector.RS = RectangleSelector(ax, line_select_callback,
                                               drawtype='box', useblit=True,
                                               button=[1], interactive=True)
        fig.canvas.mpl_connect('key_press_event', toggle_selector)

        tm = fig.canvas.manager.toolmanager
        tm.add_tool("Info (hover)", NewTool)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("Info (hover)"), "toolgroup")

        tm.add_tool("QGIS - Punkter fra plot", NewTool2)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Punkter fra plot"), "toolgroup")

        tm.add_tool("QGIS - Marker punkter på plot", NewTool3)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Marker punkter på plot"), "toolgroup")

    plt.show()

class GeophysicalLog:
    def __init__(self, x_min_val, y_min_val, x_max_val, y_max_val, data_type_val):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.x_min = x_min_val
        self.y_min = y_min_val
        self.x_max = x_max_val
        self.y_max = y_max_val
        self.data_type = data_type_val

    def fetch_log(self, srid=25832):
        sql_log = f'''
            WITH 
                tmp AS 
                    (
                        SELECT 
                            b.boreholeno, 
                            ld.dataset, ld.curveno, 
                            CASE
                                WHEN lc.logtype = 'GAMMA'
                                    then concat(lc.logtype, '_' || COALESCE(lc.logname, 'GAMM')) 
                                ELSE lc.logtype
                                END AS logtype_name,
                            lc.unit,
                            ld."depth", ld.value,
                            lc.logtype, lc.logname
                        FROM gerda.loghea lh 
                        INNER JOIN mstgerda.log_boreholes b USING (boreholeno)
                        INNER JOIN gerda.logcurve lc USING (dataset)
                        INNER JOIN gerda.logdata ld USING (dataset, curveno) 
                        WHERE b.geom && 
                            ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
                    ),
                tmp2 AS 
                    (
                        SELECT DISTINCT 
                            logtype_name
                        FROM tmp
                        ORDER BY logtype_name
                    ),
                tmp3 AS 
                    (
                        SELECT 
                            ROW_NUMBER() OVER () AS fig_number,
                            logtype_name
                        FROM tmp2
                    )      
            SELECT 
                boreholeno, 
                logtype_name,
                unit,
                dataset, curveno,
                "depth", value, 
                logtype, logname,
                fig_number
            FROM tmp 
            INNER JOIN tmp3 USING (logtype_name)
            ORDER BY dataset, curveno, "depth"
        '''

        db = Database(database='GERDA')
        df_log = db.sql_to_df(sql_log)
    #    print(df_log)
        return df_log

    def plot_log(self):
        df_log = self.fetch_log()
        df_log.sort_values(['boreholeno', 'logtype_name', 'dataset'], ascending=[True, True, True], inplace=True)
        df_log_grp = df_log.groupby(['dataset', 'curveno', 'logtype_name'])

        try:
            num_grps = int(df_log['fig_number'].max())
        except:
            num_grps = 1
        fig, axs = plt.subplots(1, num_grps, sharey='all', figsize=[16,8])
        fig.suptitle('Geophysical log', fontweight='bold')
        names = df_log['boreholeno'].unique().tolist()
        k = []
        for i in names:
            j = i.replace(' ', '')
            k.append(j)

        n = len(k)
        color = cm.jet(np.linspace(0, 1, n))


        for key, grp in df_log_grp:
               # c = next(color)
                fig_number = int(grp['fig_number'].drop_duplicates()) - 1
                title_1 = grp['logtype'].drop_duplicates().to_string(index=False)
                title_2 = grp['logname'].drop_duplicates().to_string(index=False)
                label_x = grp['unit'].drop_duplicates().to_string(index=False)
                legends = grp['boreholeno'].drop_duplicates().to_string(index=False)
                legends = legends.replace(" ", "")

                if num_grps > 1:
                    ax = axs[fig_number]
                else:
                    ax = axs

                index = k.index(legends)
                c = color[index]

                # plot data within specific logtype figure
                line,= ax.plot(grp['value'], grp['depth'], label=legends, color=c)

        #    ax.legend(loc='lower center')
                ax.set_title(f'{title_1} \n {title_2}')
                if fig_number == 0:
                    ax.set_ylabel('Depth [m]')
                ax.set_xlabel(label_x)
                ax.grid('both')

        from matplotlib.legend_handler import HandlerLineCollection, HandlerTuple

        lines_labels = [ax.get_legend_handles_labels() for ax in fig.axes]
        handles, labels = [sum(lol, []) for lol in zip(*lines_labels)]


        newLabels, newHandles = [], []
        for handle, label in zip(handles, labels):
            if label not in newLabels:
                newLabels.append(label)
                newHandles.append(handle)


        empty_lists = [[] for _ in range(len(newLabels))]

        for i in handles:
            z = i.get_label()
            z2 = newLabels.index(z)
            empty_lists[z2].append(i)

        empty_lists = [tuple(x) for x in empty_lists]

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        leg = fig.legend(empty_lists, newLabels, bbox_to_anchor=(1, 0.5))

        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), empty_lists):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            for i in range(len(origline)):
                visible = not origline[i].get_visible()
                origline[i].set_visible(visible)
                legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()

        class NewTool(ToolBase):
                description = 'Geofysiske logs tilgængelige. De er opdelt og farvet efter boringen. Legenden er interaktiv.'

        tm = fig.canvas.manager.toolmanager
        tm.add_tool("Info (hover)", NewTool)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("Info (hover)"), "toolgroup")

        fig.canvas.mpl_connect('pick_event', on_pick)
        plt.gca().invert_yaxis()

        new_borehul = [str(e) for e in newLabels]
        new_borehul = str(new_borehul).replace('[', '').replace(']', '')

        sql_lith = f'''
                        WITH 
                        waterlevel as(
            	                        SELECT DISTINCT ON (wl.WATLEVELID)
                                        b.boreholeno,
                                        b.boreholeid,
                                        wl.intakeno,
                                        wl.guid_intake,
                                        wl.watlevgrsu AS vandst_mut, --Det målte vandspejl beregnet som meter under terrænoverfladen
                                        wl.watlevmsl AS vandstkote,  --Det målte vandspejl beregnet som meter over havniveau (online DVR90 ved download det valgt kotesystem)
                                        wl.timeofmeas::DATE AS pejledato,
                                        wl.situation,
                                        b.XUTM32EUREF89 as X,
                                        b.YUTM32EUREF89 as Y,
                                        b.geom
                                        FROM jupiter.borehole b
                                        INNER JOIN jupiter.watlevel wl USING (boreholeid)
                                        WHERE replace(b.boreholeno, ' ', '') IN ({new_borehul})
                                        ORDER BY wl.WATLEVELID, pejledato DESC),
                        vand as (
                        SELECT DISTINCT ON (guid_intake)
            	  	        row_number() OVER () AS row_id,
            	                *
            	            FROM waterlevel
            	             ORDER BY guid_intake, boreholeno, pejledato DESC NULLS LAST
                        ),

                            tmp AS 
                                (
                                    SELECT 
                                        replace(b.boreholeno, ' ', '') as boreholeno,
                                        b.boreholeid, sc.top as topfilter, sc.bottom as bottomfilter,
                                        ls.top, 
                                        wl.vandst_mut as waterlevel,
                                        COALESCE(
                                            ls.bottom, 
                                            lag(ls.top, -1) OVER (PARTITION BY boreholeid ORDER BY ls.top), 
                                            b.drilldepth, 
                                            ls.top+10
                                            ) AS bottom, 
                                        ls.rocksymbol
                                    FROM jupiter.lithsamp ls
                                    INNER JOIN jupiter.borehole b USING (boreholeid)
                                    Left JOIN vand wl using (boreholeid)
                                    left join jupiter.screen sc USING (boreholeid)
                                    WHERE replace(b.boreholeno, ' ', '') IN ({new_borehul})
                                ),
                            tmp2 AS 
                                (
                                    SELECT DISTINCT 
                                        boreholeno
                                    FROM tmp
                                    ORDER BY boreholeno
                                ),
                            tmp3 AS 
                                (
                                    SELECT 
                                        ROW_NUMBER() OVER () AS fig_number,
                                        boreholeno
                                    FROM tmp2
                                )
                        SELECT 
                            tmp3.fig_number,
                            boreholeno, 
                            boreholeid, 
                            top, 
                            bottom,
                            bottomfilter,
                            topfilter,
                            waterlevel,
                            rocksymbol,
                            CASE 
                                WHEN RIGHT(rocksymbol, 1) = 's'
                                    THEN '#ff5400'
                                WHEN RIGHT(rocksymbol, 1) = 'g'
                                    THEN '#ff5400'
                                WHEN RIGHT(rocksymbol, 1) = 'q'
                                    THEN '#ff00ff'
                                WHEN RIGHT(rocksymbol, 1) = 'a'
                                    THEN '#ff8000'
                                WHEN RIGHT(rocksymbol, 1) = 'r'    
                                    THEN '#ccb3ff'
                                WHEN rocksymbol = 'ml'
                                    THEN '#dbb800'
                                WHEN rocksymbol = 'dl'
                                    THEN '#ffba33'
                                WHEN rocksymbol IN ('ol', 'pl', 'll')
                                    THEN '#000cff'
                                WHEN rocksymbol = 'gl'
                                    THEN '#b3ffff'
                                WHEN rocksymbol = 'ed'
                                    THEN '#52ffff'
                                WHEN rocksymbol NOT IN ('dl', 'ml', 'ol', 'pl', 'll')
                                    AND RIGHT(rocksymbol, 1) = 'l'
                                    THEN '#9d711f' 
                                WHEN RIGHT(rocksymbol, 1) = 'j'
                                    THEN '#9d711f'
                                WHEN RIGHT(rocksymbol, 1) = 'i'
                                    THEN '#faf59e'
                                WHEN RIGHT(rocksymbol, 1) = 'k'
                                    THEN '#80ff99'
                                WHEN RIGHT(rocksymbol, 1) = 't'
                                    THEN '#deff00'   
                                WHEN RIGHT(rocksymbol, 1) = 'p'
                                    THEN '#c16544'  
                                ELSE '#efefef'
                                END AS html_color
                        FROM tmp
                        INNER JOIN tmp3 USING (boreholeno)
                        ORDER BY boreholeno, top
                    '''

        db = Database(database='JUPITER')
        df_lith = db.sql_to_df(sql_lith)

        df_lith_grp = df_lith.groupby(['boreholeno'])

        try:
            num_grps = int(df_lith['fig_number'].max())
        except:
            num_grps = 1
        fig2, axs2 = plt.subplots(1, num_grps, sharey='all', figsize=[10,8])
        fig2.suptitle('Lithology log', fontweight='bold')
        for key, grp in df_lith_grp:
            i = 0
            for row_index, row in grp.iterrows():
                # print(row['rocksymbol'], row['top'], row['bottom'], row['html_color'])
                fig_number = row['fig_number'] - 1
                if num_grps > 1:
                    ax2 = axs2[fig_number]
                else:
                    ax2 = axs2
                # print(f"ax.fill_between(x=[0, 1], y1={row['bottom']}, y2={row['top']}, color={row['html_color']})")
                ax2.fill_between(x=[0, 1], y1=row['bottom'], y2=row['top'], color=row['html_color'])
                ax2.get_xaxis().set_visible(False)
                ax2.set_title(f'DGU:{key}')
                if row["topfilter"] is None:
                    pass
                else:
                    ax2.vlines(x=0.99991, ymin=row["topfilter"], ymax=row["bottomfilter"],
                              color='black', linewidth=4, label='Filter')

                if row["waterlevel"] is None:
                    pass
                else:
                    ax2.plot(0.99991, row["waterlevel"], color='blue', marker='<', markersize=10)

                # waterlevel = row["waterlevel"]

                print(grp)
                # plt.plot(1.01, waterlevel, color='blue', marker='<', markersize=8)

                if fig_number == 0:
                    ax2.set_ylabel('Depth [m]')

                depth = grp['bottom'].max()
                if row['bottom'] == depth:
                    ax2.text(0.5, row['bottom'], row['rocksymbol'], fontsize=9)
                else:
                    j = int(i + 1)
                    if grp.iloc[j]['rocksymbol'] != row['rocksymbol']:
                        ax2.text(0.5, row['bottom'], row['rocksymbol'], fontsize=9)
                i += 1



        plt.gca().invert_yaxis()
        plt.show()

class LithologyLog:
    def __init__(self, x_min_val, y_min_val, x_max_val, y_max_val, data_type_val):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.x_min = x_min_val
        self.y_min = y_min_val
        self.x_max = x_max_val
        self.y_max = y_max_val
        self.data_type = data_type_val

    def fetch_lithology(self, srid=25832):
        sql_lith = f'''
                WITH 
                waterlevel as(
    	                        SELECT DISTINCT ON (wl.WATLEVELID)
                                b.boreholeno,
                                b.boreholeid,
                                wl.intakeno,
                                wl.guid_intake,
                                wl.watlevgrsu AS vandst_mut, --Det målte vandspejl beregnet som meter under terrænoverfladen
                                wl.watlevmsl AS vandstkote,  --Det målte vandspejl beregnet som meter over havniveau (online DVR90 ved download det valgt kotesystem)
                                wl.timeofmeas::DATE AS pejledato,
                                wl.situation,
                                b.XUTM32EUREF89 as X,
                                b.YUTM32EUREF89 as Y,
                                b.geom
                                FROM jupiter.borehole b
                                INNER JOIN jupiter.watlevel wl USING (boreholeid)
                                WHERE b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
                                ORDER BY wl.WATLEVELID, pejledato DESC),
                vand as (
                SELECT DISTINCT ON (guid_intake)
    	  	        row_number() OVER () AS row_id,
    	                *
    	            FROM waterlevel
    	             ORDER BY guid_intake, boreholeno, pejledato DESC NULLS LAST
                ),

                    tmp AS 
                        (
                            SELECT 
                                replace(b.boreholeno, ' ', '') as boreholeno,
                                b.boreholeid, sc.top as topfilter, sc.bottom as bottomfilter,
                                ls.top, 
                                wl.vandst_mut as waterlevel,
                                b.drilendate,
                                COALESCE(
                                    ls.bottom, 
                                    lag(ls.top, -1) OVER (PARTITION BY boreholeid ORDER BY ls.top), 
                                    b.drilldepth, 
                                    ls.top+10
                                    ) AS bottom, 
                                ls.rocksymbol
                            FROM jupiter.lithsamp ls
                            INNER JOIN jupiter.borehole b USING (boreholeid)
                            Left JOIN vand wl using (boreholeid)
                            left join jupiter.screen sc USING (boreholeid)
                            WHERE b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
                        ),
                    tmp2 AS 
                        (
                            SELECT DISTINCT 
                                boreholeno
                            FROM tmp
                            ORDER BY boreholeno
                        ),
                    tmp3 AS 
                        (
                            SELECT 
                                ROW_NUMBER() OVER () AS fig_number,
                                boreholeno
                            FROM tmp2
                        )
                SELECT 
                    tmp3.fig_number,
                    boreholeno, 
                    boreholeid, 
                    top, 
                    bottom,
                    drilendate,
                    bottomfilter,
                    topfilter,
                    waterlevel,
                    rocksymbol,
                    CASE 
                        WHEN RIGHT(rocksymbol, 1) = 's'
                            THEN '#ff5400'
                        WHEN RIGHT(rocksymbol, 1) = 'g'
                            THEN '#ff5400'
                        WHEN RIGHT(rocksymbol, 1) = 'q'
                            THEN '#ff00ff'
                        WHEN RIGHT(rocksymbol, 1) = 'a'
                            THEN '#ff8000'
                        WHEN RIGHT(rocksymbol, 1) = 'r'    
                            THEN '#ccb3ff'
                        WHEN rocksymbol = 'ml'
                            THEN '#dbb800'
                        WHEN rocksymbol = 'dl'
                            THEN '#ffba33'
                        WHEN rocksymbol IN ('ol', 'pl', 'll')
                            THEN '#000cff'
                        WHEN rocksymbol = 'gl'
                            THEN '#b3ffff'
                        WHEN rocksymbol = 'ed'
                            THEN '#52ffff'
                        WHEN rocksymbol NOT IN ('dl', 'ml', 'ol', 'pl', 'll')
                            AND RIGHT(rocksymbol, 1) = 'l'
                            THEN '#9d711f' 
                        WHEN RIGHT(rocksymbol, 1) = 'j'
                            THEN '#9d711f'
                        WHEN RIGHT(rocksymbol, 1) = 'i'
                            THEN '#faf59e'
                        WHEN RIGHT(rocksymbol, 1) = 'k'
                            THEN '#80ff99'
                        WHEN RIGHT(rocksymbol, 1) = 't'
                            THEN '#deff00'   
                        WHEN RIGHT(rocksymbol, 1) = 'p'
                            THEN '#c16544'  
                        ELSE '#efefef'
                        END AS html_color
                FROM tmp
                INNER JOIN tmp3 USING (boreholeno)
                ORDER BY boreholeno, top
            '''

        print(sql_lith)

        db = Database(database='JUPITER')
        df_lith = db.sql_to_df(sql_lith)

        return df_lith

    def plot_lithology(self):
        df_lith = self.fetch_lithology()
        df_lith_grp = df_lith.groupby(['boreholeno'])

        try:
            num_grps = int(df_lith['fig_number'].max())
        except:
            num_grps = 1
        fig, axs = plt.subplots(1, num_grps, sharey='all')
        fig.suptitle('Lithology log', fontweight='bold')
        import matplotlib as mpl
        class StatusbarHoverManager:
            def __init__(self, ax):
                assert isinstance(ax, mpl.axes.Axes)

                def hover(event):
                    if event.inaxes != ax:
                        return
                    info = 'x={:.2f}, y={:.2f}'.format(event.xdata, event.ydata)
                    ax.format_coord = lambda x, y: info

                cid = ax.figure.canvas.mpl_connect("motion_notify_event", hover)

                self.ax = ax
                self.cid = cid
                self.artists = []
                self.labels = []
                self.labels2 = []

            def add_artist_labels(self, artist, label, label2):
                if isinstance(artist, list):
                    assert len(artist) == 1
                    artist = artist[0]

                self.artists += [artist]
                self.labels += [label]
                self.labels2 += [label2]

                def hover(event):
                    if event.inaxes != self.ax:
                        return
                    info = 'x={:.2f}, y={:.2f}'.format(event.xdata, event.ydata)
                    for aa, artist in enumerate(self.artists):
                        cont, dct = artist.contains(event)
                        if not cont:
                            continue
                        inds = dct.get('ind')
                        if inds is not None:  # artist contains items
                            for ii in inds:
                                lbl = self.labels[aa][ii]
                                lbl2 = self.labels2[aa][ii]

                                info += ';   DGU: {:} - {:}'.format(
                                    lbl, lbl2)
                        else:
                            lbl = self.labels[aa]
                            lbl2 = self.labels2[aa]

                            info += ';   DGU: {:} - {:}'.format(lbl, lbl2)
                    self.ax.format_coord = lambda x, y: info

                self.ax.figure.canvas.mpl_disconnect(self.cid)
                self.cid = self.ax.figure.canvas.mpl_connect(
                    "motion_notify_event", hover)

        for key, grp in df_lith_grp:
            i = 0
            for row_index, row in grp.iterrows():
                # print(row['rocksymbol'], row['top'], row['bottom'], row['html_color'])
                fig_number = row['fig_number'] - 1
                if num_grps > 1:
                    ax = axs[fig_number]
                else:
                    ax = axs
                # print(f"ax.fill_between(x=[0, 1], y1={row['bottom']}, y2={row['top']}, color={row['html_color']})")
                ax.fill_between(x=[0, 1], y1=row['bottom'], y2=row['top'], color=row['html_color'])
                ax.get_xaxis().set_visible(False)
                ax.set_title(f'DGU:{key}')
                if row["topfilter"] is None:
                    pass
                else:
                    ax.vlines(x=0.99991, ymin=row["topfilter"], ymax=row["bottomfilter"],
                              color='black', linewidth=4, label='Filter')

                if row["waterlevel"] is None:
                    pass
                else:
                    ax.plot(0.99991, row["waterlevel"], color='blue', marker='<', markersize=10)

                # waterlevel = row["waterlevel"]

                print(grp)
                # plt.plot(1.01, waterlevel, color='blue', marker='<', markersize=8)

                if fig_number == 0:
                    ax.set_ylabel('Depth [m]')

                depth = grp['bottom'].max()
                if row['bottom'] == depth:
                    ax.text(0.5, row['bottom'], row['rocksymbol'], fontsize=9)
                else:
                    j = int(i + 1)
                    if grp.iloc[j]['rocksymbol'] != row['rocksymbol']:
                        ax.text(0.5, row['bottom'], row['rocksymbol'], fontsize=9)
                i += 1

                shm = StatusbarHoverManager(ax)
                shm.add_artist_labels(ax, key, str(row["drilendate"]).split(' ')[0])

        plt.gca().invert_yaxis()
        plt.show()

class SulfatKlorid:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""
    def __init__(self, x_min_val, y_min_val, x_max_val, y_max_val, data_type_val):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.x_min = x_min_val
        self.y_min = y_min_val
        self.x_max = x_max_val
        self.y_max = y_max_val
        self.data_type = data_type_val

    def fetch_chemsamp(self, srid=25832):
        """

        :param srid:
        :return:
        """
        sql_wl = f'''
        WITH 
        prøver AS (
            SELECT *
            FROM jupiter.borehole b
            WHERE b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ),
            
        chemsampcrosstabjson AS
        (SELECT 	
			sampleid,
		   	jsonb_object_agg(compoundno, mængde ORDER BY compoundno) AS samples_json
		FROM (
		     	SELECT 
		     		gca.sampleid, 
		     		gca.compoundno, 
		     		CASE
						WHEN gca.attribute LIKE '<%' 
							OR gca.attribute LIKE 'A%' 
							OR gca.attribute LIKE 'o%' 
							OR gca.attribute LIKE '0%' 
							THEN 0::NUMERIC
						WHEN gca.attribute LIKE 'B%'
							OR gca.attribute LIKE 'C%'
							OR gca.attribute LIKE 'D%' 
							OR gca.attribute LIKE 'S%' 
							OR gca.attribute LIKE '!%' 
							OR gca.attribute LIKE '/%' 
							OR gca.attribute LIKE '*%' 
							OR gca.attribute LIKE '>%' 
							THEN -99::NUMERIC
						ELSE gca.amount
						END AS mængde
		        FROM jupiter.grwchemanalysis gca
		   ) as s
		   INNER JOIN jupiter.grwchemsample using (sampleid)
		   INNER JOIN prøver p using (boreholeid)
		GROUP BY sampleid
	),
	 inorganiccrosstab AS (
	 WITH json_to_crosstab AS 
				(
					SELECT 
						sampleid,
						(samples_json ->> '242')::NUMERIC AS Ammonium_N ,
						(samples_json ->> '280')::NUMERIC AS Calcium,
						(samples_json ->> '56')::NUMERIC AS Carbonat,
						(samples_json ->> '297')::NUMERIC AS Chlorid,
						(samples_json ->> '308')::NUMERIC AS Fluorid,
						(samples_json ->> '59')::NUMERIC AS Hydrogencarbonat,
						(samples_json ->> '312')::NUMERIC AS Jern,
						(samples_json ->> '317')::NUMERIC AS Kalium,
						(samples_json ->> '321')::NUMERIC AS Magnesium,
						(samples_json ->> '322')::NUMERIC AS Mangan,
						(samples_json ->> '356')::NUMERIC AS Methan,
						(samples_json ->> '324')::NUMERIC AS Natrium,
						(samples_json ->> '246')::NUMERIC AS Nitrat,
						(samples_json ->> '13')::NUMERIC AS pH,
						(samples_json ->> '335')::NUMERIC AS Sulfat
					FROM chemsampcrosstabjson
				)
			SELECT 
				t.*
			FROM json_to_crosstab AS t
		),
		
    chem_samp AS 
		(
			SELECT 	
				cs.sampleid,
				cs.guid_intake,
			 	concat(bh.boreholeno, '_', take.intakeno) AS DGU_Indtag,
				bh.boreholeno,
				take.stringno,
		        take.intakeno,
				cs.sampledate,
				cs.sampledate::DATE AS Dato,
				bh.use,
				bh.purpose,
				s.top AS dybde_filtertop,
				s.bottom AS dybde_filterbund,
				bh.elevation - s.top AS kote_filtertop,
				bh.elevation - s.bottom AS kote_filterbund,
				bh.xutm32euref89,
				bh.yutm32euref89,  
				bh.geom
			FROM jupiter.grwchemsample cs
			LEFT JOIN jupiter.intake take ON cs.guid_intake = take.guid 
			LEFT  JOIN jupiter.screen s ON cs.guid_intake = s.guid 
			INNER JOIN jupiter.borehole bh ON cs.guid_borehole = bh.guid 
			WHERE bh.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
			ORDER BY Dato DESC
		),

				samples AS 
					(
						SELECT
							cs.*,
							ic.chlorid AS klorid_mgprl,	
							ic.sulfat AS sulfat_mgprl
						FROM inorganiccrosstab ic
						INNER JOIN chem_samp cs USING (sampleid)
						WHERE ic.chlorid IS NOT NULL
							AND ic.sulfat IS NOT NULL
							AND cs.geom IS NOT NULL
					),
				kemi as (	
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
)
        
                SELECT DISTINCT ON (ns.guid_intake) 
                ns.guid_intake,                 
                concat(REPLACE(ns.boreholeno, ' ', ''), '_', ns.intakeno) AS dgu_intake,
                ns.sampledate, 
                ns.klorid_mgprl as klorid,
                ns.sulfat_mgprl as sulfat,
                concat(z.layer_num, '_', z.litho) AS fohm_layer,
                s.top as filtertop,
                b.purpose,
                b.use,
                b.xutm32euref89,
                b.yutm32euref89
            FROM kemi ns
            INNER JOIN jupiter.grwchemsample gcs USING (sampleid)
            INNER JOIN jupiter.borehole b USING (boreholeid)
            INNER JOIN jupiter.screen s using (boreholeid)
            LEFT JOIN temp_simak.fohm_filter_lith_aquifer z ON gcs.boreholeid = z.boreholeid
            WHERE ns.klorid_mgprl > 75
                AND ns.sulfat_mgprl > 20
                AND COALESCE(gcs.watertype, 0) NOT IN (1,3,4,5)
                AND b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ORDER BY ns.guid_intake, ns.sampledate DESC
            ;
        '''

        db = Database(database='JUPITER')
        df_wl = db.sql_to_df(sql_wl)

        return df_wl

    def plot_sulfatklorid(self):
        """

        :return:
        """
        df_wl = self.fetch_chemsamp()
        df_wl['length'] = df_wl['fohm_layer'].str.len()
        df_wl['fohm_layer'] = df_wl['fohm_layer'].replace('_', 'Ukendt lag')
        df_wl.sort_values(['length', 'fohm_layer'], ascending=[True, True], inplace=True)
        df_wl_grp = df_wl.groupby(['fohm_layer'], sort=False)

        pd.set_option('display.max_rows', 500)
        pd.set_option('display.max_columns', 500)
        pd.set_option('display.width', 1000)

        fig, ax = plt.subplots(figsize=[12,8])

        n = sum(df_wl_grp["fohm_layer"].nunique())
        color = iter(cm.rainbow(np.linspace(0, 1, n)))

        for name, grp in df_wl_grp:
            c = next(color)
            line, = ax.plot(grp['klorid'].astype(np.float), grp['sulfat'].astype(np.float), marker='o',
                            linestyle='', label=name, c=c)


        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        lines = ax.get_lines()
        leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5))
        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()

        plt.grid(b=None, which='both', axis='both')
        plt.xlim(-3, (max(df_wl['klorid'].astype(np.float))+5))
        plt.ylim(-3, (max(df_wl['sulfat'].astype(np.float))+5))
        plt.axline((0, 0), slope=1/7, color="blue", linestyle=(0, (5, 5)))

        plt.ylabel('Sulfat [mg/L]', fontweight='bold', fontsize=12)
        plt.xlabel('Klorid [mg/L]', fontweight='bold', fontsize=12)

        fig.canvas.mpl_connect('pick_event', on_pick)
        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        def update_annot(line, annot, ind):
            posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]

            gid = df_wl.loc[(df_wl['klorid'] == posx) & (df_wl['sulfat'] == posy), 'dgu_intake'].iloc[0]
            gid2 = df_wl.loc[(df_wl['klorid'] == posx) & (df_wl['sulfat'] == posy), 'purpose'].iloc[0]
            gid3 = df_wl.loc[(df_wl['klorid'] == posx) & (df_wl['sulfat'] == posy), 'use'].iloc[0]

            annot.xycoords = line.axes.transData
            annot.xy = (posx, posy)
            text = f'{gid} ({gid2} - {gid3})'
            annot.set_text(text)
            annot.get_bbox_patch().set_alpha(1)

        def hover(event):
            vis = annot.get_visible()
            if event.inaxes in [ax]:
                for line in lines:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot(line, annot, ind['ind'][0])
                        annot.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot.set_visible(False)
                            fig.canvas.draw_idle()

        fig.canvas.mpl_connect('motion_notify_event', hover)
        def onclicker(event):
            for line in lines:
                if line.contains(event)[0]:
                        cont, ind = line.contains(event)
                        annot2 = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                                     bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                                     arrowprops=dict(arrowstyle="->"))
                        update_annot(line, annot2, ind['ind'][0])
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', onclicker)

        def clearAnn( event):
            if event.inaxes is None:  # event.inaxes returns None if you click outside of the plot area, i.e. in the grey part surrounding the axes
                for ann in ax.get_children():
                    if isinstance(ann, matplotlib.text.Annotation):
                        ann.set_visible(False)
                        event.canvas.draw()
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', clearAnn)
        fig_txt = f'Polygon: {self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}'
        print(fig_txt)

        plt.figtext(0.01, 0.95, fig_txt, fontsize=6, weight='bold')

        class NewTool(ToolBase):
                description = 'Sulfat målinger over 20 mg/L og klorid målinger over 75 mg/L. \nDen stiplede linje på nedenstående figur viser forholdet mellem sulfat og klorid i havvand. \nOver den stiplede linje bidrager andre kilder, som pyritoxidation, til indholdet af sulfat, \nmens for datapunkter under den stiplede linje kan havvand forklare sulfatkoncentrationerne.  \nLegenden er interaktiv og du kan "hover" over et punkt og få boringens DGU-nummer, formål og brug.\n\nVed at indæstte punkter til QGIS (knap), kan du tegne en rektangel på plottet og de vælges herefter i QGIS.'

        class NewTool2(ToolBase):
            description = 'Indhente alle punkter fra plottet.'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                df_wl.to_csv('C:/Temp/sulfatklorid_qgis.csv', index=False)

                path = 'file:///' + 'C:/Temp/sulfatklorid_qgis.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                "latin0", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")
                #  "?delimiter=%s&crs=epsg:3857&xField=%s&yField=%s" % (",", "lon", "lat")

                mylayer = QgsVectorLayer(path, "Sulfat/klorid", "delimitedtext")

                project.addMapLayer(mylayer)

        class NewTool3(ToolBase):
            description = 'Marker valgte punkter fra QGIS i plottet. (Kør efter "QGIS - Punkter fra plot")'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                try:
                    z = ax.collections
                    z.pop(0)
                    z.remove()
                except:
                    print("No such layer..")


                if len(project.mapLayersByName('Sulfat/klorid')) == 0:
                    QMessageBox.warning(None, 'Meddelse', "Sulfat/klorid skal først indhentes.")
                    pass
                else:
                    scatterlag = project.mapLayersByName('Sulfat/klorid')[0]

                    valgtepunkter_features = scatterlag.selectedFeatures()

                    lyr = project.mapLayersByName('Sulfat/klorid')[0]

                    cols = [f.name() for f in lyr.fields()]

                    df2 = pd.DataFrame(valgtepunkter_features, columns=cols)
                    plt.autoscale(False)
                    scat = plt.scatter(df2['klorid'], df2['sulfat'], s=120, facecolors='none', edgecolors='black')

                    plt.ion()
                    plt.draw()

        def line_select_callback(eclick, erelease):
            x1, y1 = eclick.xdata, eclick.ydata
            x2, y2 = erelease.xdata, erelease.ydata
            xdata = df_wl['klorid']
            ydata = df_wl['sulfat']

            mask = (xdata > min(x1, x2)) & (xdata < max(x1, x2)) & \
                   (ydata > min(y1, y2)) & (ydata < max(y1, y2))
            print(mask)

            listen = []
            for ind, m in enumerate(mask):
                if m == True:
                    pass
                else:
                    listen.append(ind)

            df3 = df_wl.drop(listen)
            markeringer = df3['dgu_intake'].tolist()
            markeringer2 = str(markeringer).replace('[', '').replace(']', '')
            print(markeringer)
            print(markeringer2)

            project = QgsProject.instance()

            scatterlag = project.mapLayersByName('Sulfat/klorid')[0]

            expr = QgsExpression("\"dgu_intake\" IN ({})".format(str(markeringer2)))

            # it = scatterlag.getFeatures( QgsFeatureRequest( expr ) )
            ids = QgsFeatureRequest(QgsExpression(expr)).setFlags(QgsFeatureRequest.NoGeometry).setSubsetOfAttributes(
                [])
            selection = scatterlag.getFeatures(ids)  # = [i.id() for i in it]
            scatterlag.selectByIds([s.id() for s in selection])
            # scatterlag.setSelectedFeatures(selection)

        def toggle_selector(event):
            print(' Key pressed.')
            if event.key in ['Q', 'q'] and toggle_selector.RS.active:
                print(' RectangleSelector deactivated.')
                toggle_selector.RS.set_active(False)
            if event.key in ['A', 'a'] and not toggle_selector.RS.active:
                print(' RectangleSelector activated.')
                toggle_selector.RS.set_active(True)

        toggle_selector.RS = RectangleSelector(ax, line_select_callback,
                                               drawtype='box', useblit=True,
                                               button=[1], interactive=True)
        fig.canvas.mpl_connect('key_press_event', toggle_selector)

        tm = fig.canvas.manager.toolmanager
        tm.add_tool("Info (hover)", NewTool)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("Info (hover)"), "toolgroup")

        tm.add_tool("QGIS - Punkter fra plot", NewTool2)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Punkter fra plot"), "toolgroup")

        tm.add_tool("QGIS - Marker punkter på plot", NewTool3)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Marker punkter på plot"), "toolgroup")

        plt.show()

##-----------------------------------------------------------Fred's workspace
##---------------------------------------------------------------------------
##---------------------------------------------------------------------------
##---------------------------------------------------------------------------
##---------------------------------------------------------------------------
class VulnerabilityStatistics:
    """class handles the plotting of vulnerability data from fohm and pcjupiterxl"""
    def __init__(self, x_min_val, y_min_val, x_max_val, y_max_val, data_type_val):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.x_min = x_min_val
        self.y_min = y_min_val
        self.x_max = x_max_val
        self.y_max = y_max_val
        self.data_type = data_type_val

    def fetch_vulnstats(self, srid=25832):
        """

        :param srid:
        :return:
        """
        sql_vs = f'''
        with 	fohm_depth as ( --get the depth of fohm and top point of fohm
			select
			flba.boreholeid,
            flba.geom,
			scr.guid, 
			scr.top as filtertop,
			flba.fdepth,
			flba.terraintop
			from (
			select
			flb.boreholeid,
            flb.geom,
			max(-flb.layer_bot)-min(-flb.layer_top) as fdepth,
			max(flb.layer_top) as terraintop
			from fohm_borehole.fohm_layer_borehole_geom flb
            where flb.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
			group by flb.boreholeid, flb.geom
			) flba
			right join jupiter_fdw.screen_view scr using (boreholeid) where flba.boreholeid is not null
		),
		bh_ler as (	 --make temporary table with categorisation of clay layers and thicknesses
			SELECT distinct on (fd.guid,ls.top)
			ls.boreholeid,
            fd.geom,
			fd.guid,
			fd.fdepth,
			fd.filtertop,
			case  --in the unlikely case that fohm is shallower than borehole, cut the borehole at depth
				when (ls.bottom) < fd.fdepth /*bottom of borehole layer is above total fohm depth*/ 
					then  ls.bottom-ls.top 
				when (ls.top) > fd.fdepth /* top of the borehole layer is deeper than total fohm*/
					then 0
				else fd.fdepth - ls.top /*fohm total depth - borehole layer top*/
			end as tykkelse,
			case 
				when fd.filtertop is null -- filter does not exist
					then null
				when fd.filtertop < fd.fdepth /*bottom of borehole layer is above total fohm depth*/ 
					then case
						when (ls.bottom) < fd.filtertop
							then ls.bottom-ls.top
						when (ls.top) > fd.filtertop
							then 0
						else fd.filtertop - ls.top
					end
				else 0
			end as tykkelse_over_filter
			FROM jupiter_fdw.lithsamp_view ls
			inner join fohm_borehole.fohm_layer_borehole flb using (boreholeid)
			inner join fohm_depth fd using (boreholeid)
			where ls.rocksymbol in (
				'al', 'bl', 'cj', 'cl', 'dl',						
				'ed', 'el', 'fl', 'gl', 'hl',
				'i', 'il', 'j', 'jj', 'jl',
				'kl', 'l', 'll', 'ml', 'nj',
				'nl', 'ol', 'pj', 'pl', 'ql',
				'rj', 'rl', 'sj', 'tl', 'uj',
				'ul', 'vj', 'vl', 'wl', 'xl',
				'yl', 'zl'
				)
				and ls.bottom is not null and ls.top is not null
		),
		bh_ler_tykkelse as ( --make temporary table with total thickness of clay in borehole
			select distinct on (l.guid)
			l.guid,
            l.geom,
			l.fdepth,
			l.filtertop,
			la.tykkelse_i_boring,
			la.tykkelse_over_filter_i_boring
			from (
				select 
				l.guid,
				sum(l.tykkelse) as tykkelse_i_boring,
				sum(l.tykkelse_over_filter) as tykkelse_over_filter_i_boring
				from bh_ler l
				group by l.guid --group by boreholeid to sum all clay types together and all non-clay types together
			) la 
			join bh_ler l using (guid)
		),
		tot_bor as ( --get the bottom of borehole relative to fohm placement
			select distinct on (fd.guid)
			ls.boreholeid,
			fd.guid,
			ta.bottom-fd.terraintop as bottom,
			ta.filtertop-fd.terraintop as filtertop,
			ta.bottom as bordepth
			from (
				select
				ls.boreholeid,
				max(ls.bottom) as bottom, --find bottom of borehole in fohm
				min(fd.filtertop) as filtertop
				from jupiter_fdw.lithsamp_view ls
				inner join fohm_depth fd using (boreholeid)
				group by ls.boreholeid
			) ta 
			join jupiter_fdw.lithsamp_view ls using (boreholeid)
			join fohm_depth fd using (boreholeid)
		),
		foh_ler as ( --make temporary table with categorisation of clay layers and thickness
			select distinct on (tb.guid,flb.layer_top)
			ls.boreholeid,
			tb.guid,
			tb.bordepth,
			tb.filtertop,
			case  --cut fohm layer if below bottom of borehole
				when (-flb.layer_bot) < tb.bottom /*bottom of fohm layer is above borehole total depth*/ 
					then  flb.layer_top - flb.layer_bot
				when (-flb.layer_top) > tb.bottom /* top of the fohm layer is deeper than the borehole*/
					then 0
				else tb.bottom - (-flb.layer_top) /*borehole total depth-fohm.layer_top*/
			end as tykkelse,
			case 
				when tb.filtertop is null 
					then null
				when (-flb.layer_bot) < tb.filtertop
					then flb.layer_top - flb.layer_bot
				when (-flb.layer_top) > tb.filtertop
					then 0
				else tb.filtertop - (-flb.layer_top)
			end as tykkelse_over_filter
			from fohm_borehole.fohm_layer_borehole flb
			inner join jupiter_fdw.lithsamp_view ls using (boreholeid)
			left join tot_bor tb using (boreholeid)
			where flb.litho = 'Clay' and tb.bottom is not null
		),
		fohm_ler_tykkelse as ( --make temporary table with total thickness of clay in fohm
			select distinct on (f.guid)
			f.guid, 
			f.bordepth,
			fa.tykkelse_i_fohm,
			fa.tykkelse_over_filter_i_fohm,
			f.filtertop
			from (
				select
				f.guid,
				sum(f.tykkelse) as tykkelse_i_fohm,
				sum(f.tykkelse_over_filter) as tykkelse_over_filter_i_fohm
				from foh_ler f
				group by f.guid
			) fa join foh_ler f using (guid)
		)
select --final table
	scr.boreholeid,
	scr.boreholeno,
	scr.guid,
    wt.dgu_indtag,
	bh.XUTM32EUREF89,
	bh.YUTM32EUREF89,
	least (fl.bordepth,bl.fdepth) as afskæring, -- fohmdybde eller boringsdybde, whichever is shallower (FRA TOP AF BORING, ikke kote)
	fl.bordepth as boringsdybde,--(FRA TOP AF BORING, ikke kote)
	bl.tykkelse_i_boring,
	fl.tykkelse_i_fohm,
	case
		when bl.tykkelse_i_boring is null or bl.tykkelse_i_boring=0
			then -1 --simplified equation
		when fl.tykkelse_i_fohm is null or fl.tykkelse_i_fohm=0
			then 1 --simplified equation
		when (bl.tykkelse_i_boring+fl.tykkelse_i_fohm)=0
			then null
		else (bl.tykkelse_i_boring-fl.tykkelse_i_fohm)/(bl.tykkelse_i_boring+fl.tykkelse_i_fohm) --relative difference
	end as reldiff_total,
	least (bl.filtertop,bl.fdepth) as afskæring_filter, -- fohmdybde eller filterdybde, whichever is shallower (FRA TOP AF BORING, ikke kote)
	bl.filtertop as filtertop_mut,--(FRA TOP AF BORING, ikke kote)
	fl.filtertop as filtertop_kote,
	bl.tykkelse_over_filter_i_boring,
	fl.tykkelse_over_filter_i_fohm,
	case
		when (bl.tykkelse_over_filter_i_boring is null or bl.tykkelse_over_filter_i_boring=0) and (fl.tykkelse_over_filter_i_fohm is null or fl.tykkelse_over_filter_i_fohm=0)
			then null
		when bl.tykkelse_over_filter_i_boring is null or bl.tykkelse_over_filter_i_boring=0
			then -1 --simplified equation
		when fl.tykkelse_over_filter_i_fohm is null or fl.tykkelse_over_filter_i_fohm=0
			then 1 --simplified equation
		when (bl.tykkelse_over_filter_i_boring+fl.tykkelse_over_filter_i_fohm)=0
			then null
		else (bl.tykkelse_over_filter_i_boring-fl.tykkelse_over_filter_i_fohm)/(bl.tykkelse_over_filter_i_boring+fl.tykkelse_over_filter_i_fohm) --relative difference
	end as reldiff_overfilter,
	wt.vandtype,
	case
            when vandtype = 'A' and tykkelse_over_filter_i_boring < 5 then true
            when vandtype = 'B' and tykkelse_over_filter_i_boring < 5 then true
            when vandtype = 'AB' and tykkelse_over_filter_i_boring < 5 then true
            when vandtype = 'X' and tykkelse_over_filter_i_boring < 5 then true
            when vandtype = 'Y' and tykkelse_over_filter_i_boring between 5 and 15 then true
            when vandtype = 'C1' and tykkelse_over_filter_i_boring > 5 then true
            when vandtype = 'C2' and tykkelse_over_filter_i_boring between 5 and 15 then true
            when vandtype = 'D' and tykkelse_over_filter_i_boring > 15 then true
            else false
        end vandtype_ks_boring,
     case
            when vandtype = 'A' and tykkelse_over_filter_i_fohm < 5 then true
            when vandtype = 'B' and tykkelse_over_filter_i_fohm < 5 then true
            when vandtype = 'AB' and tykkelse_over_filter_i_fohm < 5 then true
            when vandtype = 'X' and tykkelse_over_filter_i_fohm < 5 then true
            when vandtype = 'Y' and tykkelse_over_filter_i_fohm between 5 and 15 then true
            when vandtype = 'C1' and tykkelse_over_filter_i_fohm > 5 then true
            when vandtype = 'C2' and tykkelse_over_filter_i_fohm between 5 and 15 then true
            when vandtype = 'D' and tykkelse_over_filter_i_fohm > 15 then true
            else false
        end vandtype_ks_fohm,
      nt.nitrat_mgprl 
from jupiter_fdw.screen_view scr -- så vi får alle filtre med
left join bh_ler_tykkelse bl using (guid)
left join fohm_ler_tykkelse fl using (guid)
left join jupiter_fdw.mstmvw_substance_watertype_latest_view wt using (guid_intake)
left join jupiter_fdw.mstmvw_substance_nitrate_latest_view nt using (guid_intake)
left join jupiter_fdw.borehole_view bh using (boreholeid)
where bl.tykkelse_i_boring is not null or fl.tykkelse_i_fohm is not null -- kun boringer hvor der er ler i enten jupiter eller fohm
    and bl.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
        '''

        db = Database(database='FOHM')
        df_vs = db.sql_to_df(sql_vs)

        return df_vs
    
    def plot_vulnstat(self):
        """

        :return:
        """
        df_vs = self.fetch_vulnstats()

        fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True,figsize=(13,13))
        
        im1=ax1.scatter(df_vs['tykkelse_over_filter_i_boring'],df_vs['nitrat_mgprl'],c=df_vs['filtertop_mut'],cmap='gist_rainbow',s=20,vmin=0,vmax=120,picker=True,pickradius=5)
        im2=ax2.scatter(df_vs['tykkelse_over_filter_i_boring'],df_vs['tykkelse_over_filter_i_fohm'],c=df_vs['filtertop_mut'],cmap='gist_rainbow',s=20,vmin=0,vmax=120,picker=True,pickradius=5)
        
        df2=df_vs[['tykkelse_over_filter_i_boring','tykkelse_over_filter_i_fohm']]
        rval_1=df2.corr(method='pearson',min_periods=5)
        rval=rval_1.iat[0,1]
        r2val=rval**2
        maxval=np.max([np.max(df_vs['tykkelse_over_filter_i_boring']),np.max(df_vs['tykkelse_over_filter_i_fohm'])])
        line=mlines.Line2D([0,maxval],[0,maxval],color='red')
        ax2.add_line(line)
        right = 0.8
        top = 0.9
        rtext='r= {:.2f} , r_squared= {:.2f}'.format(rval, r2val)
        ax2.text(right, top, rtext,
        horizontalalignment='right',
        verticalalignment='top',
        transform=ax2.transAxes)
        
        if np.max(df_vs['nitrat_mgprl'])==0:
                nitax=5
        else:
                nitax=np.max(df_vs['nitrat_mgprl'])*1.1
        if (np.max(df_vs['tykkelse_over_filter_i_boring'])==0) & (np.max(df_vs['tykkelse_over_filter_i_fohm'])==0):
                lerax=10
        else:
            lerax=np.max([np.max(df_vs['tykkelse_over_filter_i_boring']),np.max(df_vs['tykkelse_over_filter_i_fohm'])])
        
        ax1.set_xlim(0,lerax)
        ax1.set_ylim(0,nitax)
        ax2.set_xlim(0,lerax)
        ax2.set_ylim(0,lerax)
        
        plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
        cax = plt.axes([0.85, 0.1, 0.075, 0.8])
        cbar=fig.colorbar(im1, cax=cax)
        cbar.set_label('Dybde til filtertop [m]')
        ax1.set(xlabel='Lertykkelse over filter, JUPITER [m]',ylabel='Nitratindhold [mgpl]')
        ax2.set(xlabel='Lertykkelse over filter, JUPITER [m]',ylabel='Lertykkelse over filter, FOHM [m]')
        
             
        annot = ax1.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
        ax1.figure.texts.append(ax1.texts.pop())
        annot.set_visible(False)
        
        annot2 = ax2.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
        ax2.figure.texts.append(ax2.texts.pop())
        annot2.set_visible(False)
        
        def onpick1(event):
            artist = event.artist
            if artist.axes == ax1:
                ind = event.ind
                posx=df_vs.tykkelse_over_filter_i_boring[ind].iloc[0]
                posy=df_vs.nitrat_mgprl[ind].iloc[0]
                dgu=df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx) & (df_vs['nitrat_mgprl'] == posy), 'boreholeno'].iloc[0]
                url='https://data.geus.dk/JupiterWWW/borerapport.jsp?dgunr={}'.format(dgu.replace(" ",""))
                webbrowser.open(url)
            if artist.axes == ax2:
                ind = event.ind
                posx=df_vs.tykkelse_over_filter_i_boring[ind].iloc[0]
                posy=df_vs.tykkelse_over_filter_i_fohm[ind].iloc[0]
                dgu=df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx) & (df_vs['tykkelse_over_filter_i_fohm'] == posy), 'boreholeno'].iloc[0]
                url='https://data.geus.dk/JupiterWWW/borerapport.jsp?dgunr={}'.format(dgu.replace(" ",""))
                webbrowser.open(url)
                
        fig.canvas.mpl_connect('pick_event', onpick1)
        
        def update_annot(ind,flag):
            #if mouse-over left plot
            if flag==1:
                [posx1, posy1] = im1.get_offsets()[ind["ind"][0]]
                
                indtag=df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx1) & (df_vs['nitrat_mgprl'] == posy1), 'dgu_indtag'].iloc[0]
                bhid=df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx1) & (df_vs['nitrat_mgprl'] == posy1), 'boreholeno'].iloc[0]
                if pd.isnull(indtag):
                    gid=bhid
                else:
                    gid=indtag
                gid2 = df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx1) & (df_vs['nitrat_mgprl'] == posy1), 'tykkelse_over_filter_i_boring'].iloc[0]
                gid3 = df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx1) & (df_vs['nitrat_mgprl'] == posy1), 'nitrat_mgprl'].iloc[0]
                im2posy= df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx1) & (df_vs['nitrat_mgprl'] == posy1), 'tykkelse_over_filter_i_fohm'].iloc[0]
                filt=df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx1) & (df_vs['nitrat_mgprl'] == posy1), 'filtertop_mut'].iloc[0]
                
                annot.xy = (posx1, posy1)
                text = 'DGU nr: {} (filtertop {:.2f}m.u.t.) \n{:.2f}m ler over filter i JUPITER, {:.2f}mgpl nitrat'.format(gid, filt, gid2, gid3)
                annot.set_text(text)
                annot.get_bbox_patch().set_alpha(1)
                
                annot2.xy = (posx1, im2posy)
                text = 'DGU nr: {} (filtertop {:.2f}m.u.t.) \n{:.2f}m ler over filter i JUPITER, {:.2f}m ler over filter i FOHM'.format(gid, filt, gid2, im2posy)
                annot2.set_text(text)
                annot2.get_bbox_patch().set_alpha(1)
            if flag==2:
                [posx1, posy1] = im2.get_offsets()[ind["ind"][0]]
                
                indtag=df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx1) & (df_vs['tykkelse_over_filter_i_fohm'] == posy1), 'dgu_indtag'].iloc[0]
                bhid=df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx1) & (df_vs['tykkelse_over_filter_i_fohm'] == posy1), 'boreholeno'].iloc[0]
                if pd.isnull(indtag):
                    gid=bhid
                else:
                    gid=indtag
                gid2 = df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx1) & (df_vs['tykkelse_over_filter_i_fohm'] == posy1), 'tykkelse_over_filter_i_boring'].iloc[0]
                gid3 = df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx1) & (df_vs['tykkelse_over_filter_i_fohm'] == posy1), 'nitrat_mgprl'].iloc[0]
                im2posy= df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx1) & (df_vs['tykkelse_over_filter_i_fohm'] == posy1), 'tykkelse_over_filter_i_fohm'].iloc[0]
                filt=df_vs.loc[(df_vs['tykkelse_over_filter_i_boring'] == posx1) & (df_vs['tykkelse_over_filter_i_fohm'] == posy1), 'filtertop_mut'].iloc[0]
                
                annot.xy = (posx1, gid3)
                text = 'DGU nr: {} (filtertop {:.2f}m.u.t.) \n{:.2f}m ler over filter i JUPITER, {:.2f}mgpl nitrat'.format(gid, filt, gid2, gid3)
                annot.set_text(text)
                annot.get_bbox_patch().set_alpha(1)
                
                annot2.xy = (posx1, posy1)
                text = 'DGU nr: {} (filtertop {:.2f}m.u.t.) \n{:.2f}m ler over filter i JUPITER, {:.2f}m ler over filter i FOHM'.format(gid, filt, gid2, im2posy)
                annot2.set_text(text)
                annot2.get_bbox_patch().set_alpha(1)
        
        
        def hover(event):
            vis = annot.get_visible()
            if event.inaxes == ax1:
                cont, ind = im1.contains(event)
                if cont:
                    update_annot(ind,1)
                    annot.set_visible(True)
                    annot2.set_visible(True)
                    fig.canvas.draw_idle()
                else:
                    if vis:
                        annot.set_visible(False)
                        annot2.set_visible(False)
                        fig.canvas.draw_idle()
            if event.inaxes == ax2:
                cont, ind = im2.contains(event)
                if cont:
                    update_annot(ind,2)
                    annot.set_visible(True)
                    annot2.set_visible(True)
                    fig.canvas.draw_idle()
                else:
                    if vis:
                        annot.set_visible(False)
                        annot2.set_visible(False)
                        fig.canvas.draw_idle()
        
        fig.canvas.mpl_connect("motion_notify_event", hover)
 
        fig_txt = f'Polygon: {self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}'
        print(fig_txt)
        plt.figtext(0.01, 0.95, fig_txt, fontsize=6, weight='bold')
        plt.show()
        
    def plot_vulnstat2(self):
        """

        :return:
        """
        df_vs = self.fetch_vulnstats()
        
        plt.ion()
        fig, ax = plt.subplots()
        colors = np.where(df_vs["vandtype_ks_fohm"]==True,'g','r')
        colors2 = np.where((df_vs["vandtype_ks_boring"]!=df_vs["vandtype_ks_fohm"]),'k','#FF000000')
        diff=ax.scatter(df_vs.tykkelse_over_filter_i_fohm,df_vs.filtertop_mut,s=50,edgecolors=colors2,facecolors='none')
        f=ax.scatter(df_vs.tykkelse_over_filter_i_fohm,df_vs.filtertop_mut,c=colors,s=20)
        diff.set_visible(False)

        fig.subplots_adjust(left=0.3)

        rax = fig.add_axes([0.05, 0.7, 0.15, 0.15])
        radio = RadioButtons(rax, ('FOHM', 'JUPITER'))
        rax2 = fig.add_axes([0.05, 0.6, 0.15, 0.10])
        check = CheckButtons(rax2, ['Forskel'])
        plt.show()
        
        return df_vs,f,radio,check,diff

    def tykfunc(label,f,diff,df_vs):
        def clicked(label):
            tykdict = {'FOHM': [df_vs.tykkelse_over_filter_i_fohm, df_vs.vandtype_ks_fohm], 'JUPITER': [df_vs.tykkelse_over_filter_i_boring, df_vs.vandtype_ks_boring]}
            [xdata, cdata] = tykdict[label]
            colors3 = np.where(cdata==True,'g','r')
            f.set_offsets(np.c_[xdata.loc[:],df_vs.filtertop_mut.loc[:]])
            f.set_color(colors3)
            diff.set_offsets(np.c_[xdata.loc[:],df_vs.filtertop_mut.loc[:]])
            plt.draw()
        return clicked
            
    def func(event,diff):
        def clicked(event):
            diff.set_visible(not diff.get_visible())
            plt.draw()
        return clicked
        
##---------------------------------------------------------------------------
##---------------------------------------------------------------------------
##---------------------------------------------------------------------------
##----------------------------------------------------End of Fred's workspace

class ScatterSulfatNitrat:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""
    def __init__(self, x_min_val, y_min_val, x_max_val, y_max_val, data_type_val):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.x_min = x_min_val
        self.y_min = y_min_val
        self.x_max = x_max_val
        self.y_max = y_max_val
        self.data_type = data_type_val

    def fetch_chemsamp(self, srid=25832):
        """

        :param srid:
        :return:
        """
        sql_wl = f'''
         WITH 
        prøver AS (
            SELECT *
            FROM jupiter.borehole b
            WHERE b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ),
            
        chemsampcrosstabjson AS
        (SELECT 	
			sampleid,
		   	jsonb_object_agg(compoundno, mængde ORDER BY compoundno) AS samples_json
		FROM (
		     	SELECT 
		     		gca.sampleid, 
		     		gca.compoundno, 
		     		CASE
						WHEN gca.attribute LIKE '<%' 
							OR gca.attribute LIKE 'A%' 
							OR gca.attribute LIKE 'o%' 
							OR gca.attribute LIKE '0%' 
							THEN 0::NUMERIC
						WHEN gca.attribute LIKE 'B%'
							OR gca.attribute LIKE 'C%'
							OR gca.attribute LIKE 'D%' 
							OR gca.attribute LIKE 'S%' 
							OR gca.attribute LIKE '!%' 
							OR gca.attribute LIKE '/%' 
							OR gca.attribute LIKE '*%' 
							OR gca.attribute LIKE '>%' 
							THEN -99::NUMERIC
						ELSE gca.amount
						END AS mængde
		        FROM jupiter.grwchemanalysis gca
		   ) as s
		   INNER JOIN jupiter.grwchemsample using (sampleid)
		   INNER JOIN prøver p using (boreholeid)
		GROUP BY sampleid
	),
	 inorganiccrosstab AS (
	 WITH json_to_crosstab AS 
				(
					SELECT 
						sampleid,
						(samples_json ->> '242')::NUMERIC AS Ammonium_N ,
						(samples_json ->> '280')::NUMERIC AS Calcium,
						(samples_json ->> '56')::NUMERIC AS Carbonat,
						(samples_json ->> '297')::NUMERIC AS Chlorid,
						(samples_json ->> '308')::NUMERIC AS Fluorid,
						(samples_json ->> '59')::NUMERIC AS Hydrogencarbonat,
						(samples_json ->> '312')::NUMERIC AS Jern,
						(samples_json ->> '317')::NUMERIC AS Kalium,
						(samples_json ->> '321')::NUMERIC AS Magnesium,
						(samples_json ->> '322')::NUMERIC AS Mangan,
						(samples_json ->> '356')::NUMERIC AS Methan,
						(samples_json ->> '324')::NUMERIC AS Natrium,
						(samples_json ->> '246')::NUMERIC AS Nitrat,
						(samples_json ->> '13')::NUMERIC AS pH,
						(samples_json ->> '335')::NUMERIC AS Sulfat
					FROM chemsampcrosstabjson
				)
			SELECT 
				t.*
			FROM json_to_crosstab AS t
		),
		
    chem_samp AS 
		(
			SELECT 	
				cs.sampleid,
				cs.guid_intake,
			 	concat(bh.boreholeno, '_', take.intakeno) AS DGU_Indtag,
				bh.boreholeno,
				take.stringno,
		        take.intakeno,
				cs.sampledate,
				cs.sampledate::DATE AS Dato,
				bh.use,
				bh.purpose,
				s.top AS dybde_filtertop,
				s.bottom AS dybde_filterbund,
				bh.elevation - s.top AS kote_filtertop,
				bh.elevation - s.bottom AS kote_filterbund,
				bh.xutm32euref89,
				bh.yutm32euref89,  
				bh.geom
			FROM jupiter.grwchemsample cs
			LEFT JOIN jupiter.intake take ON cs.guid_intake = take.guid 
			LEFT  JOIN jupiter.screen s ON cs.guid_intake = s.guid 
			INNER JOIN jupiter.borehole bh ON cs.guid_borehole = bh.guid 
			WHERE bh.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
			ORDER BY Dato DESC
		),

				samples AS 
					(
						SELECT
							cs.*,
							ic.nitrat AS nitrat_mgprl,	
							ic.sulfat AS sulfat_mgprl
						FROM inorganiccrosstab ic
						INNER JOIN chem_samp cs USING (sampleid)
						WHERE ic.nitrat IS NOT NULL
							AND ic.sulfat IS NOT NULL
							AND cs.geom IS NOT NULL
					),
				kemi as (	
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
)
        
        
                SELECT DISTINCT ON (ns.guid_intake) 
                ns.guid_intake, 
                concat(REPLACE(ns.boreholeno, ' ', ''), '_', ns.intakeno) AS dgu_intake,
                ns.sampledate, 
                ns.nitrat_mgprl as nitrat,
                ns.sulfat_mgprl as sulfat,
                concat(z.layer_num, '_', z.litho) AS fohm_layer,
                s.top as filtertop,
                b.purpose,
                b.use,
                b.xutm32euref89,
				b.yutm32euref89
            FROM kemi ns
            INNER JOIN jupiter.grwchemsample gcs USING (sampleid)
            INNER JOIN jupiter.borehole b USING (boreholeid)
            INNER JOIN jupiter.screen s using (boreholeid)
            LEFT JOIN temp_simak.fohm_filter_lith_aquifer z ON gcs.boreholeid = z.boreholeid
            WHERE ns.nitrat_mgprl > 0
                AND ns.sulfat_mgprl > 0
                AND COALESCE(gcs.watertype, 0) NOT IN (1,3,4,5)
                AND COALESCE(upper(b.purpose), 'K') NOT IN ('L', 'VA')
                AND b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ORDER BY ns.guid_intake, ns.sampledate DESC
            ;
        '''

        db = Database(database='JUPITER')
        df_wl = db.sql_to_df(sql_wl)

        return df_wl

    def plot_sulfatnitrat(self):
        """

        :return:
        """
        df_wl = self.fetch_chemsamp()
        df_wl['length'] = df_wl['fohm_layer'].str.len()
        df_wl['fohm_layer'] = df_wl['fohm_layer'].replace('_', 'Ukendt lag')
        df_wl.sort_values(['length', 'fohm_layer'], ascending=[True, True], inplace=True)
        df_wl_grp = df_wl.groupby(['fohm_layer'], sort=False)

        pd.set_option('display.max_rows', 500)
        pd.set_option('display.max_columns', 500)
        pd.set_option('display.width', 1000)

        from matplotlib import pyplot as plt, patches, rcParams, cm
        import numpy as np

        fig, ax = plt.subplots(figsize=[12,8])

        n = sum(df_wl_grp["fohm_layer"].nunique())
        color = iter(cm.rainbow(np.linspace(0, 1, n)))

        for name, grp in df_wl_grp:
            c = next(color)
            line, = ax.plot(grp['sulfat'].astype(np.float), grp['nitrat'].astype(np.float), marker='o',
                            linestyle='', label=name, c=c)


        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        lines = ax.get_lines()
        leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5))
        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()


        plt.grid(b=None, which='both', axis='both')
        ax.margins(y=0.05)
        ax.margins(x=0.05)
        plt.xlim([0, max(df_wl['sulfat'])+10])
        plt.axhline(y=1, color='r', linestyle='-')
        plt.axvline(x=20, color='r', linestyle='-')

        plt.text(.05, .05, "I", fontsize=11, fontweight='bold', color='#380000', ha='right', va='top', transform=ax.transAxes
                 )
        plt.text(.95, .05, "II", fontsize=11, fontweight='bold', color='#380000', ha='right', va='top', transform=ax.transAxes
                 )
        plt.text(.95, .95, "III", fontsize=11, fontweight='bold', color='#380000', ha='right', va='top', transform=ax.transAxes
                 )
        plt.text(.05, .95, "IV", fontsize=11, fontweight='bold', color='#380000', ha='right', va='top', transform=ax.transAxes
                 )
        plt.ylabel('Nitrat [mg/L]', fontweight='bold', fontsize=12)
        plt.xlabel('Sulfat [mg/L]', fontweight='bold', fontsize=12)

        ax.set_yscale('log')
        from matplotlib.ticker import ScalarFormatter
        for axis in [ax.yaxis]:
            axis.set_major_formatter(ScalarFormatter())

        fig.canvas.mpl_connect('pick_event', on_pick)
        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        def update_annot(line, annot, ind):
            posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]
            gid = df_wl.loc[(df_wl['sulfat'] == posx) & (df_wl['nitrat'] == posy), 'dgu_intake'].iloc[0]
            gid2 = df_wl.loc[(df_wl['sulfat'] == posx) & (df_wl['nitrat'] == posy), 'purpose'].iloc[0]
            gid3 = df_wl.loc[(df_wl['sulfat'] == posx) & (df_wl['nitrat'] == posy), 'use'].iloc[0]

            annot.xycoords = line.axes.transData
            annot.xy = (posx, posy)
            text = f'{gid} ({gid2} - {gid3})'
            annot.set_text(text)
            annot.get_bbox_patch().set_alpha(1)

        def hover(event):
            vis = annot.get_visible()
            if event.inaxes in [ax]:
                for line in lines:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot(line, annot, ind['ind'][0])
                        annot.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot.set_visible(False)
                            fig.canvas.draw_idle()

        fig.canvas.mpl_connect('motion_notify_event', hover)


        def onclicker(event):
            for line in lines:
                if line.contains(event)[0]:
                        cont, ind = line.contains(event)
                        annot2 = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                                     bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                                     arrowprops=dict(arrowstyle="->"))
                        update_annot(line, annot2, ind['ind'][0])
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', onclicker)

        def clearAnn( event):
            if event.inaxes is None:  # event.inaxes returns None if you click outside of the plot area, i.e. in the grey part surrounding the axes
                for ann in ax.get_children():
                    if isinstance(ann, matplotlib.text.Annotation):
                        ann.set_visible(False)
                        event.canvas.draw()
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', clearAnn)

        fig_txt = f'Polygon: {self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}'
        print(fig_txt)

        plt.figtext(0.01, 0.95, fig_txt, fontsize=6, weight='bold')


        class NewTool(ToolBase):
                description = 'Scatterplot af Nitrat og Sulfat, der kan illustrere grundvandets sårbarhed over for påvirkning fra terræn.\n\nKvadrant I: Grundvandets indhold af nitrat er under 1 mg/l og sulfat under baggrundsniveauet. \nVandtypen bestemmes her til D (eller C1 hvis baggrundsniveauet for sulfat er 20 mg/l). \n\nKvadrant II: Grundvandets indhold af sulfat ligger over baggrundsniveauet, mens nitratindholdet er under 1 mg/l. \nDet forhøjede sulfatindhold kan indikere, at vandet er påvirket af pyritoxidation. \n\nKvadrant III: Grundvandets indhold af sulfat er over baggrundsniveauet, mens nitratindholdet er over 1 mg/l. \nVandet er oxideret, hvilket viser sårbarhed over for påvirkninger fra terræn. \n\nKvadrant IV: Grundvandets indhold af nitrat er over 1 mg/l, mens sulfatindholdet er under baggrundsniveauet. \nVandtypen viser både tegn på sulfatreduktion og oxiderende forhold. . \n\nLegenden er interaktiv og du kan "hover" over et punkt og få boringens DGU-nummer, formål og brug. \n\n Ved at indæstte punkter til QGIS (knap), kan du tegne en rektangel på plottet og de vælges herefter i QGIS.'

        class NewTool2(ToolBase):
            description = 'Indhente alle punkter fra scatterplottet.'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                df_wl.to_csv('C:/Temp/scatternitratsulfat_qgis.csv', index=False)

                path = 'file:///' + 'C:/Temp/scatternitratsulfat_qgis.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % ("latin0",",", "xutm32euref89", "yutm32euref89","epsg:25832")
                #  "?delimiter=%s&crs=epsg:3857&xField=%s&yField=%s" % (",", "lon", "lat")

                mylayer = QgsVectorLayer(path, "Nitrat/sulfat - scatterplot", "delimitedtext")

                project.addMapLayer(mylayer)

        class NewTool3(ToolBase):
            description = 'Marker valgte punkter fra QGIS i plottet. (Kør efter "QGIS - Punkter fra plot")'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                try:
                    z = ax.collections
                    z.pop(0)
                    z.remove()

                except:
                    print("No such layer..")

                if len(project.mapLayersByName('Nitrat/sulfat - scatterplot')) == 0:
                    QMessageBox.warning(None, 'Meddelse', "Nitrat/sulfat - scatterplot skal først indhentes.")
                    pass
                else:
                    scatterlag = project.mapLayersByName('Nitrat/sulfat - scatterplot')[0]

                    valgtepunkter_features = scatterlag.selectedFeatures()

                    lyr = project.mapLayersByName('Nitrat/sulfat - scatterplot')[0]

                    cols = [f.name() for f in lyr.fields()]

                    df2 = pd.DataFrame(valgtepunkter_features, columns=cols)
                    plt.autoscale(False)
                    scat = plt.scatter(df2['sulfat'], df2['nitrat'], s=120, facecolors='none', edgecolors='black')

                    plt.ion()
                    plt.draw()

        def line_select_callback(eclick, erelease):
            x1, y1 = eclick.xdata, eclick.ydata
            x2, y2 = erelease.xdata, erelease.ydata
            xdata = df_wl['sulfat']
            ydata = df_wl['nitrat']

            mask = (xdata > min(x1, x2)) & (xdata < max(x1, x2)) & \
                   (ydata > min(y1, y2)) & (ydata < max(y1, y2))
            print(mask)

            listen = []
            for ind, m in enumerate(mask):
                if m == True:
                    pass
                else:
                    listen.append(ind)

            df3 = df_wl.drop(listen)
            markeringer = df3['dgu_intake'].tolist()
            markeringer2 = str(markeringer).replace('[', '').replace(']', '')
            print(markeringer)
            print(markeringer2)

            project = QgsProject.instance()

            scatterlag = project.mapLayersByName('Nitrat/sulfat - scatterplot')[0]

            expr = QgsExpression("\"dgu_intake\" IN ({})".format(str(markeringer2)))

            #it = scatterlag.getFeatures( QgsFeatureRequest( expr ) )
            ids = QgsFeatureRequest(QgsExpression(expr)).setFlags(QgsFeatureRequest.NoGeometry).setSubsetOfAttributes([])
            selection = scatterlag.getFeatures(ids)#= [i.id() for i in it]
            scatterlag.selectByIds([s.id() for s in selection])
            #scatterlag.setSelectedFeatures(selection)

        def toggle_selector(event):
            print(' Key pressed.')
            if event.key in ['Q', 'q'] and toggle_selector.RS.active:
                print(' RectangleSelector deactivated.')
                toggle_selector.RS.set_active(False)
            if event.key in ['A', 'a'] and not toggle_selector.RS.active:
                print(' RectangleSelector activated.')
                toggle_selector.RS.set_active(True)


        toggle_selector.RS = RectangleSelector(ax, line_select_callback,
                                               drawtype='box', useblit=True,
                                               button=[1], interactive=True)
        fig.canvas.mpl_connect('key_press_event', toggle_selector)

        tm = fig.canvas.manager.toolmanager
        tm.add_tool("Info (hover)", NewTool)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("Info (hover)"), "toolgroup")

        tm.add_tool("QGIS - Punkter fra plot", NewTool2)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Punkter fra plot"), "toolgroup")

        tm.add_tool("QGIS - Marker punkter på plot", NewTool3)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Marker punkter på plot"), "toolgroup")

        plt.show()


class Chemtable:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""
    def __init__(self, x_min_val, y_min_val, x_max_val, y_max_val, data_type_val):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.x_min = x_min_val
        self.y_min = y_min_val
        self.x_max = x_max_val
        self.y_max = y_max_val
        self.data_type = data_type_val

    def fetch_chemsamp(self, srid=25832):
        """

        :param srid:
        :return:
        """
        sql_wl = f'''
         WITH 
        prøver AS (
            SELECT *
            FROM jupiter.borehole b
            WHERE b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ),
            
        chemsampcrosstabjson AS
        (SELECT 	
			sampleid,
		   	jsonb_object_agg(compoundno, mængde ORDER BY compoundno) AS samples_json
		FROM (
		     	SELECT 
		     		gca.sampleid, 
		     		gca.compoundno, 
		     		CASE
						WHEN gca.attribute LIKE '<%' 
							OR gca.attribute LIKE 'A%' 
							OR gca.attribute LIKE 'o%' 
							OR gca.attribute LIKE '0%' 
							THEN 0::NUMERIC
						WHEN gca.attribute LIKE 'B%'
							OR gca.attribute LIKE 'C%'
							OR gca.attribute LIKE 'D%' 
							OR gca.attribute LIKE 'S%' 
							OR gca.attribute LIKE '!%' 
							OR gca.attribute LIKE '/%' 
							OR gca.attribute LIKE '*%' 
							OR gca.attribute LIKE '>%' 
							THEN -99::NUMERIC
						ELSE gca.amount
						END AS mængde
		        FROM jupiter.grwchemanalysis gca
		   ) as s
		   INNER JOIN jupiter.grwchemsample using (sampleid)
		   INNER JOIN prøver p using (boreholeid)
		GROUP BY sampleid
	),
	 inorganiccrosstab AS (
	 WITH json_to_crosstab AS 
				(
					SELECT 
						sampleid,
						(samples_json ->> '242')::NUMERIC AS Ammonium_N ,
						(samples_json ->> '280')::NUMERIC AS Calcium,
						(samples_json ->> '56')::NUMERIC AS Carbonat,
						(samples_json ->> '297')::NUMERIC AS Chlorid,
						(samples_json ->> '308')::NUMERIC AS Fluorid,
						(samples_json ->> '59')::NUMERIC AS Hydrogencarbonat,
						(samples_json ->> '312')::NUMERIC AS Jern,
						(samples_json ->> '317')::NUMERIC AS Kalium,
						(samples_json ->> '321')::NUMERIC AS Magnesium,
						(samples_json ->> '322')::NUMERIC AS Mangan,
						(samples_json ->> '356')::NUMERIC AS Methan,
						(samples_json ->> '324')::NUMERIC AS Natrium,
						(samples_json ->> '246')::NUMERIC AS Nitrat,
						(samples_json ->> '50')::NUMERIC AS Oxygen_indhold,
						(samples_json ->> '13')::NUMERIC AS pH,
						(samples_json ->> '335')::NUMERIC AS Sulfat
					FROM chemsampcrosstabjson
				)
			SELECT 
				t.*
			FROM json_to_crosstab AS t
		),
		
    chem_samp AS 
		(
			SELECT 	
				cs.sampleid,
				cs.guid_intake,
			 	concat(bh.boreholeno, '_', take.intakeno) AS DGU_Indtag,
				bh.boreholeno,
				take.stringno,
		        take.intakeno,
				cs.sampledate,
				cs.sampledate::DATE AS Dato,
				bh.use,
				bh.purpose,
				s.top AS dybde_filtertop,
				s.bottom AS dybde_filterbund,
				bh.elevation - s.top AS kote_filtertop,
				bh.elevation - s.bottom AS kote_filterbund,
				bh.xutm32euref89,
				bh.yutm32euref89,  
				bh.geom
			FROM jupiter.grwchemsample cs
			LEFT JOIN jupiter.intake take ON cs.guid_intake = take.guid 
			LEFT  JOIN jupiter.screen s ON cs.guid_intake = s.guid 
			INNER JOIN jupiter.borehole bh ON cs.guid_borehole = bh.guid 
			WHERE bh.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
			ORDER BY Dato DESC
		),

				samples AS 
					(
						SELECT
							cs.*,
							ic.chlorid,	
							ic.sulfat,
							ic.nitrat,
							ic.oxygen_indhold,
							ic.jern,
							ic.mangan, ic.methan, ic.ammonium_n, ic.calcium, ic.fluorid, ic.magnesium, ic.ph, ic.kalium, ic.hydrogencarbonat,
							ic.natrium
						FROM inorganiccrosstab ic
						INNER JOIN chem_samp cs USING (sampleid)
						WHERE ic.chlorid IS NOT NULL
							AND ic.sulfat IS NOT NULL
							AND cs.geom IS NOT NULL
					),
				kemi as (	
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
)  
            SELECT DISTINCT ON (ns.guid_intake) 
                ns.guid_intake, 
                ns.sampledate, 
                ns.nitrat as nitrat,
                ns.sulfat as sulfat,
                ns.oxygen_indhold as ilt,
                ns.jern,
                ns.mangan,
                ns.methan,
                ns.ammonium_n,
                ns.chlorid as klorid,
                ns.calcium,
                ns.fluorid,
                ns.magnesium,
                ns.ph,
                ns.kalium,
                ns.hydrogencarbonat,
                ns.natrium,
                concat(z.layer_num, '_', z.litho) AS fohm_layer,
                s.top as filtertop,
                concat(REPLACE(ns.boreholeno, ' ', ''), '_', ns.intakeno) AS dgu_intake
            FROM kemi ns
            INNER JOIN jupiter.grwchemsample gcs USING (sampleid)
            INNER JOIN jupiter.borehole b USING (boreholeid)
            INNER JOIN jupiter.screen s using (boreholeid)
            LEFT JOIN temp_simak.fohm_filter_lith_aquifer z ON gcs.boreholeid = z.boreholeid
            WHERE COALESCE(gcs.watertype, 0) NOT IN (1,3,4,5)
                AND COALESCE(upper(b.purpose), 'K') NOT IN ('L', 'VA')
                AND b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ORDER BY ns.guid_intake, ns.sampledate DESC
            ;
        '''

        db = Database(database='JUPITER')
        df_wl = db.sql_to_df(sql_wl)

        return df_wl

    def plot_table(self):
        """

        :return:
        """
        from matplotlib.font_manager import FontProperties
        from matplotlib import pyplot as plt, patches, rcParams, cm
        import numpy as np

        df_wl = self.fetch_chemsamp()
        df_wl = df_wl.fillna(value=np.nan)
        fig=plt.figure(figsize=(10, 4.5)) #fig er ikke udnævnt i denne funktion. Regner med det er her det mangler /fsh
        pd.set_option('display.max_rows', 500)
        pd.set_option('display.max_columns', 500)
        pd.set_option('display.width', 1000)

        print(df_wl)

        d = {'Hovedbestanddele': ['Nitrat', 'Oxygen', 'Sulfat', 'Jern','Mangan','Methan','Ammonium','Klorid','Calcium','Fluorid','Magnesium','pH', 'Natrium', 'Hydrogencarbonat', 'Kalium'],
             'Gennemsnit [mg/L]': [np.mean(df_wl["nitrat"]), np.mean(df_wl["ilt"]), np.mean(df_wl["sulfat"]), np.mean(df_wl["jern"]), np.mean(df_wl["mangan"]), np.mean(df_wl["methan"]),
                                   np.mean(df_wl["ammonium_n"]), np.mean(df_wl["klorid"])
                                                                                   , np.mean(df_wl["calcium"]), np.mean(df_wl["fluorid"]), np.mean(df_wl["magnesium"]), np.mean(df_wl["ph"])
                                                                                , np.mean(df_wl["natrium"]), np.mean(df_wl["hydrogencarbonat"]), np.mean(df_wl["kalium"])],

             'Std. Afvigelse' : [np.std(df_wl["nitrat"]), np.std(df_wl["ilt"]), np.std(df_wl["sulfat"]), np.std(df_wl["jern"])
                 , np.std(df_wl["mangan"]), np.std(df_wl["methan"]), np.std(df_wl["ammonium_n"]), np.std(df_wl["klorid"])
                 , np.std(df_wl["calcium"]), np.std(df_wl["fluorid"]), np.std(df_wl["magnesium"]), np.std(df_wl["ph"])
                 , np.std(df_wl["natrium"]), np.std(df_wl["hydrogencarbonat"]), np.std(df_wl["kalium"])
                                 ], 'Antal prøver' : [np.count_nonzero(~np.isnan(df_wl["nitrat"])),
            np.count_nonzero(~np.isnan(df_wl["ilt"])), np.count_nonzero(~np.isnan(df_wl["sulfat"])), np.count_nonzero(~np.isnan(df_wl["jern"]))
                                                      , np.count_nonzero(~np.isnan(df_wl["mangan"])), np.count_nonzero(~np.isnan(df_wl["methan"])), np.count_nonzero(~np.isnan(df_wl["ammonium_n"]))
                                                      , np.count_nonzero(~np.isnan(df_wl["klorid"])), np.count_nonzero(~np.isnan(df_wl["calcium"])), np.count_nonzero(~np.isnan(df_wl["fluorid"]))
                                                      , np.count_nonzero(~np.isnan(df_wl["magnesium"])), np.count_nonzero(~np.isnan(df_wl["ph"]))
                                                      , np.count_nonzero(~np.isnan(df_wl["natrium"])), np.count_nonzero(~np.isnan(df_wl["hydrogencarbonat"])), np.count_nonzero(~np.isnan(df_wl["kalium"]))],
             'Kvalitetskrav': [50, '-', 250, 0.1, 0.02, 0.01, 0.05, 250, '≈200', 1.5, 50, '7-8.5', 175, 30, 10]}

        df = pd.DataFrame(data=d)
        df = df.loc[df["Antal prøver"] != 0]
        df = df.sort_values(df.columns[0], ascending = True)
        df = df.round(2)

        table = plt.table(cellText=df.values, colLabels=df.columns, loc='center')
        table.auto_set_font_size(False)
        table.set_fontsize(10)

        for (row, col), cell in table.get_celld().items():
            cell.set_text_props(ha="center")
            if (col == 0):
                cell.set_text_props(fontproperties=FontProperties(weight='bold'))
                cell.set_facecolor("#1ac3f5")
            if (col == 4):
                cell.set_facecolor("#AAAAAA")
            if (row == 0):
                cell.set_text_props(fontproperties=FontProperties(weight='bold'))
                cell.set_facecolor("#56b5fd")


        plt.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
        plt.tick_params(axis='y', which='both', right=False, left=False, labelleft=False)
        for pos in ['right', 'top', 'bottom', 'left']:
            plt.gca().spines[pos].set_visible(False)

        class NewTool(ToolBase):
                description = 'Tabel over seneste målinger.'
        tm = fig.canvas.manager.toolmanager
        tm.add_tool("Info (hover)", NewTool)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("Info (hover)"), "toolgroup")

        plt.tight_layout()
        plt.show()

class DybdePlotVandType:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""
    def __init__(self, x_min_val, y_min_val, x_max_val, y_max_val, data_type_val):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.x_min = x_min_val
        self.y_min = y_min_val
        self.x_max = x_max_val
        self.y_max = y_max_val
        self.data_type = data_type_val

    def fetch_chemsamp(self, srid=25832):
        """

        :param srid:
        :return:
        """
        sql_wl = f'''
        WITH 
        prøver AS (
            SELECT *
            FROM jupiter.borehole b
            WHERE b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ),
            
        chemsampcrosstabjson AS
        (SELECT 	
			sampleid,
		   	jsonb_object_agg(compoundno, mængde ORDER BY compoundno) AS samples_json
		FROM (
		     	SELECT 
		     		gca.sampleid, 
		     		gca.compoundno, 
		     		CASE
						WHEN gca.attribute LIKE '<%' 
							OR gca.attribute LIKE 'A%' 
							OR gca.attribute LIKE 'o%' 
							OR gca.attribute LIKE '0%' 
							THEN 0::NUMERIC
						WHEN gca.attribute LIKE 'B%'
							OR gca.attribute LIKE 'C%'
							OR gca.attribute LIKE 'D%' 
							OR gca.attribute LIKE 'S%' 
							OR gca.attribute LIKE '!%' 
							OR gca.attribute LIKE '/%' 
							OR gca.attribute LIKE '*%' 
							OR gca.attribute LIKE '>%' 
							THEN -99::NUMERIC
						ELSE gca.amount
						END AS mængde
		        FROM jupiter.grwchemanalysis gca
		   ) as s
		   INNER JOIN jupiter.grwchemsample using (sampleid)
		   INNER JOIN prøver p using (boreholeid)
		GROUP BY sampleid
	),
	 inorganiccrosstab AS (
	 WITH json_to_crosstab AS 
				(
					SELECT 
						sampleid,
						(samples_json ->> '242')::NUMERIC AS Ammonium_N ,
						(samples_json ->> '280')::NUMERIC AS Calcium,
						(samples_json ->> '56')::NUMERIC AS Carbonat,
						(samples_json ->> '297')::NUMERIC AS Chlorid,
						(samples_json ->> '308')::NUMERIC AS Fluorid,
						(samples_json ->> '59')::NUMERIC AS Hydrogencarbonat,
						(samples_json ->> '312')::NUMERIC AS Jern,
						(samples_json ->> '317')::NUMERIC AS Kalium,
						(samples_json ->> '321')::NUMERIC AS Magnesium,
						(samples_json ->> '322')::NUMERIC AS Mangan,
						(samples_json ->> '356')::NUMERIC AS Methan,
						(samples_json ->> '324')::NUMERIC AS Natrium,
						(samples_json ->> '246')::NUMERIC AS Nitrat,
						(samples_json ->> '50')::NUMERIC AS Oxygen_indhold,
						(samples_json ->> '13')::NUMERIC AS pH,
						(samples_json ->> '335')::NUMERIC AS Sulfat
					FROM chemsampcrosstabjson
				)
			SELECT 
				t.*
			FROM json_to_crosstab AS t
		),
		
    chem_samp AS 
		(
			SELECT 	
				cs.sampleid,
				cs.guid_intake,
			 	concat(bh.boreholeno, '_', take.intakeno) AS DGU_Indtag,
				bh.boreholeno,
				take.stringno,
		        take.intakeno,
				cs.sampledate,
				cs.sampledate::DATE AS Dato,
				bh.use,
				bh.purpose,
				s.top AS dybde_filtertop,
				s.bottom AS dybde_filterbund,
				bh.elevation - s.top AS kote_filtertop,
				bh.elevation - s.bottom AS kote_filterbund,
				bh.xutm32euref89,
				bh.yutm32euref89,  
				bh.geom
			FROM jupiter.grwchemsample cs
			LEFT JOIN jupiter.intake take ON cs.guid_intake = take.guid 
			LEFT  JOIN jupiter.screen s ON cs.guid_intake = s.guid 
			INNER JOIN jupiter.borehole bh ON cs.guid_borehole = bh.guid 
			WHERE bh.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
			ORDER BY Dato DESC
		),

				samples AS 
					(
						SELECT
							cs.*,
							ic.nitrat AS Nitrat_mgprl,		
							ic.jern AS Jern_mgprl,	
							ic.sulfat AS sulfat_mgprl,
							ic.oxygen_indhold AS Ilt_mgprl,
							ic.mangan AS mangan_mgprl,
							ic.ammonium_n AS ammonium_n_mgprl,
							CASE
					            WHEN 
						            ic.nitrat > 1
									AND ic.jern >= 0.2 
						            THEN 'X'::text
								WHEN 
						            ic.nitrat > 1 
						            AND ic.jern < 0.2
									AND ic.jern >=0
									AND (ic.oxygen_indhold IS NULL)-- OR ic.oxygen_indhold <= 0)
						            THEN 'AB'::text
								WHEN 
						            ic.nitrat > 1 
						            AND ic.jern < 0.2
									AND ic.jern >= 0
						            AND ic.oxygen_indhold < 1 
						            THEN 'B'::text
								WHEN 
						            ic.nitrat > 1 
						            AND ic.jern < 0.2
									AND ic.jern >= 0
						            AND ic.oxygen_indhold >= 1 
						            THEN 'A'::text
								WHEN
									ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern < 0.2
									AND ic.jern >= 0
									AND ic.oxygen_indhold >= 1
									AND ic.mangan < 0.01
									AND ic.mangan >= 0
									AND ic.ammonium_n < 0.01
									AND ic.ammonium_n >= 0
						            THEN 'AY'::text
					            WHEN 
						            ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern < 0.2
									AND ic.jern >= 0
						            THEN 'Y'::text
					            WHEN 
						            ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern >= 0.2
						            AND ic.sulfat < 20
									AND ic.sulfat >= 0
						            THEN 'D'::text
					            WHEN 
						            ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern >= 0.2
						            AND ic.sulfat >= 20 AND ic.sulfat < 70 
									THEN 'C1'::text
								WHEN
									ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern >= 0.2
						            AND ic.sulfat >= 70  
						            THEN 'C2'::text
					            ELSE NULL
								END AS vandtype
						FROM inorganiccrosstab ic
						INNER JOIN chem_samp cs USING (sampleid)
						WHERE ic.jern IS NOT NULL
							AND ic.nitrat IS NOT NULL
							AND ic.sulfat IS NOT NULL
							AND cs.geom IS NOT NULL
					),
				kemi as (	
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
)  
            SELECT DISTINCT ON (vt.guid_intake) 
                vt.guid_intake,
                concat(REPLACE(vt.boreholeno, ' ', ''), '_', vt.intakeno) AS dgu_intake, 
                vt.sampledate, 
                vt.vandtype,
                concat(z.layer_num, '_', z.litho) AS fohm_layer,
                s.top as filtertop,
                b.purpose,
                b.use,
                b.xutm32euref89,
				b.yutm32euref89
            FROM kemi vt
            INNER JOIN jupiter.grwchemsample gcs USING (sampleid)
            INNER JOIN jupiter.grwchemanalysis gca USING (sampleid) 
            INNER JOIN jupiter.borehole b USING (boreholeid)
            INNER JOIN jupiter.screen s using (boreholeid)
            LEFT JOIN temp_simak.fohm_filter_lith_aquifer z ON gcs.boreholeid = z.boreholeid
            WHERE vt.vandtype IS NOT NULL
                AND COALESCE(gcs.watertype, 0) NOT IN (1,3,4,5)
                AND COALESCE(upper(b.purpose), 'K') NOT IN ('L', 'VA')
                AND b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ORDER BY vt.guid_intake, vt.sampledate DESC
            ;
        '''

        db = Database(database='JUPITER')
        df_wl = db.sql_to_df(sql_wl)

        return df_wl

    def plot_dybdeplot(self):
        """

        :return:
        """
        df_wl = self.fetch_chemsamp()
        df_wl['length'] = df_wl['fohm_layer'].str.len()
        df_wl['fohm_layer'] = df_wl['fohm_layer'].replace('_', 'Ukendt lag')
        df_wl.sort_values(['length', 'fohm_layer', 'vandtype'], ascending=[True, True, True], inplace=True)
        df_wl_grp = df_wl.groupby(['fohm_layer'], sort=False)

        pd.set_option('display.max_rows', 500)
        pd.set_option('display.max_columns', 500)
        pd.set_option('display.width', 1000)

        from matplotlib import pyplot as plt, patches, rcParams, cm
        import numpy as np

        fig, ax = plt.subplots(figsize=[12,8])

        n = sum(df_wl_grp["fohm_layer"].nunique())

        color = iter(cm.rainbow(np.linspace(0, 1, n)))
        y = [1, 2, 3, 4, 5, 6, 7]
        x = ['A', 'AB', 'B', 'C2', 'C1', 'D', 'X', 'Y']
        sentinel, = ax.plot(['A', 'AB', 'B', 'C2', 'C1', 'D', 'X', 'Y'], np.linspace(min(y), max(y), len(x)))
        sentinel.remove()

        for name, grp in df_wl_grp:
            c = next(color)
            line, = ax.plot(grp['vandtype'], grp['filtertop'], marker='o',
                            linestyle='', label=name, c=c)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        lines = ax.get_lines()
        leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5))
        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()

        plt.xticks(['A', 'AB', 'B', 'C2', 'C1', 'D', 'X', 'Y'], ['A', 'AB', 'B', 'C2', 'C1', 'D', 'X', 'Y'],
                   fontsize=10, fontweight='bold')
        plt.title('Vandtype', fontweight='bold', fontsize=12)
        plt.grid(visible=None, which='both', axis='both')
        ax.margins(y=0.05)
        ax.margins(x=0.05)
        plt.ylabel('Filtertop (mut)', fontweight='bold', fontsize=12)
        plt.gca().invert_yaxis()
        ax.tick_params(labelbottom=False, labeltop=True)
        ax.xaxis.set_ticks_position('top')
        ax.tick_params(labelbottom=False, labeltop=True)

        fig.canvas.mpl_connect('pick_event', on_pick)
        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        def update_annot(line, annot, ind):
            posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]

            gid = df_wl.loc[(df_wl['vandtype'] == posx) & (df_wl['filtertop'] == posy), 'dgu_intake'].iloc[0]
            gid2 = df_wl.loc[(df_wl['vandtype'] == posx) & (df_wl['filtertop'] == posy), 'purpose'].iloc[0]
            gid3 = df_wl.loc[(df_wl['vandtype'] == posx) & (df_wl['filtertop'] == posy), 'use'].iloc[0]

            # annot.xycoords = line.axes.transData
            annot.xy = (posx, posy)
            text = f'{gid} ({gid2} - {gid3})'
            annot.set_text(text)
            annot.get_bbox_patch().set_alpha(1)

        def hover(event):
            vis = annot.get_visible()
            if event.inaxes in [ax]:
                for line in lines:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot(line, annot, ind['ind'][0])
                        annot.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot.set_visible(False)
                            fig.canvas.draw_idle()

        fig.canvas.mpl_connect('motion_notify_event', hover)
        def onclicker(event):
            for line in lines:
                if line.contains(event)[0]:
                        cont, ind = line.contains(event)
                        annot2 = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                                     bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                                     arrowprops=dict(arrowstyle="->"))
                        update_annot(line, annot2, ind['ind'][0])
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', onclicker)

        def clearAnn( event):
            if event.inaxes is None:  # event.inaxes returns None if you click outside of the plot area, i.e. in the grey part surrounding the axes
                for ann in ax.get_children():
                    if isinstance(ann, matplotlib.text.Annotation):
                        ann.set_visible(False)
                        event.canvas.draw()
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', clearAnn)
        fig_txt = f'Polygon: {self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}'
        print(fig_txt)

        plt.figtext(0.01, 0.01, fig_txt, fontsize=6, weight='bold')
        print(df_wl['vandtype'].unique())
        print(df_wl['vandtype'].value_counts())

        class NewTool(ToolBase):
                description = 'Dybdeplot af vandtyper. \n\nLegenden er interaktiv og du kan "hover" over et punkt og få boringens DGU-nummer, formål og brug. \n\n Ved at indæstte punkter til QGIS (knap), kan du tegne en rektangel på plottet og de vælges herefter i QGIS.'


        class NewTool2(ToolBase):
            description = 'Indhente alle punkter fra plottet.'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                df_wl.to_csv('C:/Temp/vandtype_qgis.csv', index=False)

                path = 'file:///' + 'C:/Temp/vandtype_qgis.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % ("latin0",",", "xutm32euref89", "yutm32euref89","epsg:25832")
                #  "?delimiter=%s&crs=epsg:3857&xField=%s&yField=%s" % (",", "lon", "lat")

                mylayer = QgsVectorLayer(path, "Vandtype", "delimitedtext")

                project.addMapLayer(mylayer)

        class NewTool3(ToolBase):
            description = 'Marker valgte punkter fra QGIS i plottet. (Kør efter "QGIS - Punkter fra plot")'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                try:
                    z = ax.collections
                    z.pop(0)
                    z.remove()

                except:
                    print("No such layer..")

                if len(project.mapLayersByName('Vandtype')) == 0:
                    QMessageBox.warning(None, 'Meddelse', "Vandtype skal først indhentes.")
                    pass
                else:
                    scatterlag = project.mapLayersByName('Vandtype')[0]

                    valgtepunkter_features = scatterlag.selectedFeatures()

                    lyr = project.mapLayersByName('Vandtype')[0]

                    cols = [f.name() for f in lyr.fields()]

                    df2 = pd.DataFrame(valgtepunkter_features, columns=cols)
                    plt.autoscale(False)
                    scat = plt.scatter(df2['vandtype'], df2['filtertop'], s=120, facecolors='none', edgecolors='black')

                    plt.ion()
                    plt.draw()

        def line_select_callback(eclick, erelease):
            x1, y1 = eclick.xdata, eclick.ydata
            x2, y2 = erelease.xdata, erelease.ydata
            xdata = df_wl['vandtype']
            xdata = xdata.replace('A', 0 ).replace('AB', 1 ).replace('B', 2 ).replace('C2', 3 ).replace('C1', 4 ).replace('D', 5 ).replace('X', 6 ).replace('Y', 7 )
            ydata = df_wl['filtertop']

            mask = (xdata > min(x1, x2)) & (xdata < max(x1, x2)) & \
                   (ydata > min(y1, y2)) & (ydata < max(y1, y2))

            listen = []
            for ind, m in enumerate(mask):
                if m == True:
                    pass
                else:
                    listen.append(ind)

            df3 = df_wl.drop(listen)
            markeringer = df3['dgu_intake'].tolist()
            markeringer2 = str(markeringer).replace('[', '').replace(']', '')
            print(markeringer)
            print(markeringer2)

            project = QgsProject.instance()

            scatterlag = project.mapLayersByName('Vandtype')[0]

            expr = QgsExpression("\"dgu_intake\" IN ({})".format(str(markeringer2)))

            #it = scatterlag.getFeatures( QgsFeatureRequest( expr ) )
            ids = QgsFeatureRequest(QgsExpression(expr)).setFlags(QgsFeatureRequest.NoGeometry).setSubsetOfAttributes([])
            selection = scatterlag.getFeatures(ids)#= [i.id() for i in it]
            scatterlag.selectByIds([s.id() for s in selection])
            #scatterlag.setSelectedFeatures(selection)

        def toggle_selector(event):
            print(' Key pressed.')
            if event.key in ['Q', 'q'] and toggle_selector.RS.active:
                print(' RectangleSelector deactivated.')
                toggle_selector.RS.set_active(False)
            if event.key in ['A', 'a'] and not toggle_selector.RS.active:
                print(' RectangleSelector activated.')
                toggle_selector.RS.set_active(True)

        toggle_selector.RS = RectangleSelector(ax, line_select_callback,
                                               drawtype='box', useblit=True,
                                               button=[1], interactive=True)

        fig.canvas.mpl_connect('key_press_event', toggle_selector)

        tm = fig.canvas.manager.toolmanager
        tm.add_tool("Info (hover)", NewTool)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("Info (hover)"), "toolgroup")

        tm.add_tool("QGIS - Punkter fra plot", NewTool2)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Punkter fra plot"), "toolgroup")

        tm.add_tool("QGIS - Marker punkter på plot", NewTool3)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Marker punkter på plot"), "toolgroup")


        plt.show()

class DybdeplotPesticiderSum:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""
    def __init__(self, x_min_val, y_min_val, x_max_val, y_max_val, data_type_val):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.x_min = x_min_val
        self.y_min = y_min_val
        self.x_max = x_max_val
        self.y_max = y_max_val
        self.data_type = data_type_val

    def fetch_samp(self, srid=25832):
        """

        :param srid:
        :return:
        """
        sql_wl = f'''
            WITH 
            prøver AS (
            SELECT *
            FROM jupiter.borehole b
            WHERE b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ),
            
                pesticide_gvk_all AS
                (
                SELECT 	
			ROW_NUMBER() over (ORDER BY gcs.boreholeno) AS ID,
			gcs.boreholeno,
			gcs.boreholeid, 
			gcs.intakeno,
			gca.sampleid,
			gcs.sampledate,
			gca.compoundno,
			cpl.long_text AS compound,
			cpl.casno,
			gca."attribute",
			CASE 
				WHEN gca.unit = 1
					THEN gca.amount * 1000
				WHEN gca.unit = 20
					THEN gca.amount
				WHEN gca.unit = 28
					THEN gca.amount * 1000000000
				WHEN gca.unit = 59
					THEN gca.amount * 1000000
				WHEN gca.unit = 155
					THEN gca.amount * 0.001
				WHEN gca.unit = 163
					THEN gca.amount * 0.000001
				ELSE NULL 
				END AS amount_µgprl,
			b.abandondat,
			pl.plantid,
			pl.plantname,
			pl.planttype,
			pl.companytype,
			pl.longtext as companytype_desc,
			gcs.project,
			b.xutm32euref89,
			b.yutm32euref89, 
			gcs.guid_intake,
			gca.guid AS guid_grwchemanalysis,
			b.geom
		FROM jupiter.grwchemanalysis gca
		LEFT JOIN 
			(
				SELECT 
					c.code::NUMERIC AS unit,
					c.longtext AS unit_descr
				FROM jupiter.code c 
				WHERE codetype = 752
			) AS unit USING (unit)
		INNER JOIN jupiter.grwchemsample gcs USING (sampleid) 
		LEFT JOIN jupiter.compoundlist cpl USING (compoundno)
		LEFT JOIN jupiter.borehole b USING (boreholeid)
		INNER JOIN 
			(
				SELECT DISTINCT 
					dpi.boreholeid, 
					dp.plantid, 
					dp.plantname, 
					dp.planttype, 
					dpct.companytype, 
					c.longtext
				FROM jupiter.drwplant dp
				INNER JOIN jupiter.drwplantcompanytype dpct USING (plantid)
				INNER JOIN jupiter.drwplantintake dpi USING (plantid)
				INNER JOIN jupiter.code c on dpct.companytype = c.code
				ORDER BY dp.plantid
			) pl ON gcs.boreholeid = pl.boreholeid
		WHERE b.geom IS NOT NULL
		AND b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
			AND gca.compoundno IN 
				(
					SELECT compoundno
					FROM jupiter.compoundgroup
					WHERE compoundgroupno = 50
				)
		ORDER BY pl.plantname, gcs.boreholeno, gcs.sampledate desc, cpl.long_text
                ),
                
			    pest_recent AS 
					(
						SELECT DISTINCT ON (guid_intake)
							guid_intake
						FROM pesticide_gvk_all
						ORDER BY guid_intake, sampledate DESC NULLS LAST 
					),
					
				pest_recent_sum AS 	
					(
						SELECT 
							sampleid,
							SUM
								(
									CASE 
										WHEN COALESCE("attribute", '>') IN ('>', 'B', 'C')
											AND amount_µgprl IS NOT NULL
											THEN amount_µgprl 
										ELSE 0
										END
								) AS sum_pesticide,
							count(*) AS number_of_pesticide
						FROM pesticide_gvk_all
						INNER JOIN pest_recent USING (guid_intake)
						GROUP BY sampleid 
					)
					
           SELECT DISTINCT ON (p.guid_intake)
				ROW_NUMBER() over () AS id,
				pr.sampleid, 
				p.boreholeid,
				pr.sum_pesticide, 
				p.guid_intake,
				b.purpose,
				b.use,
				concat(z.layer_num, '_', z.litho) AS fohm_layer,
				concat(REPLACE(p.boreholeno, ' ', ''), '_', p.intakeno) AS dgu_intake,
				s.top as filtertop,
				b.xutm32euref89,
				b.yutm32euref89
			FROM pest_recent_sum pr
			LEFT JOIN pesticide_gvk_all p USING (sampleid)
			LEFT JOIN temp_simak.fohm_filter_lith_aquifer z USING (boreholeid) 
			INNER JOIN jupiter.screen s USING (boreholeid)
			INNER JOIN jupiter.grwchemsample gcs USING (sampleid)
			LEFT JOIN jupiter.borehole b ON p.boreholeid = b.boreholeid
			WHERE b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ORDER BY p.guid_intake DESC
            ;
        '''

        db = Database(database='JUPITER')
        df_wl = db.sql_to_df(sql_wl)

        return df_wl

    def plot_dybdeplot(self):
        """

        :return:
        """
        pd.set_option('display.max_rows', 500)
        pd.set_option('display.max_columns', 500)
        pd.set_option('display.width', 1000)


        df_wl = self.fetch_samp()
        df_wl['length'] = df_wl['fohm_layer'].str.len()
        df_wl['fohm_layer'] = df_wl['fohm_layer'].replace('_', 'Ukendt lag')
        df_wl.sort_values(['length', 'fohm_layer'], ascending=[True, True], inplace=True)
        df_wl_grp = df_wl.groupby(['fohm_layer'], sort=False)


        from matplotlib import pyplot as plt, patches, rcParams, cm
        import numpy as np

        fig, ax = plt.subplots(figsize=[12,8])

        n = sum(df_wl_grp["fohm_layer"].nunique())

        color = iter(cm.rainbow(np.linspace(0, 1, n)))

        for name,grp in df_wl_grp:
                c = next(color)
                line, = ax.plot(grp['sum_pesticide'].astype(np.float),
                                grp['filtertop'].astype(np.float), marker='o',
                                linestyle='', label=name, c=c)



        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        lines = ax.get_lines()
        leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5))
        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()

        plt.axvline(x=0.5, color='r', linestyle='-')
        plt.title('Pesticider (sum) [µg/L]', fontweight='bold', fontsize=12)
        plt.grid(b=None, which='both', axis='both')
        ax.margins(y=0.05)
        plt.xlim(-0.5, (max(df_wl['sum_pesticide'].astype(np.float))+5))
        plt.ylabel('Filtertop (mut)', fontweight='bold', fontsize=12)
        plt.gca().invert_yaxis()
        ax.tick_params(labelbottom=False, labeltop=True)
        ax.xaxis.set_ticks_position('top')
        ax.tick_params(labelbottom=False, labeltop=True)

        fig.canvas.mpl_connect('pick_event', on_pick)
        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        def update_annot(line, annot, ind):
            posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]

            gid = df_wl.loc[(df_wl['sum_pesticide'] == posx) & (df_wl['filtertop'] == posy), 'dgu_intake'].iloc[0]
            gid2 = df_wl.loc[(df_wl['sum_pesticide'] == posx) & (df_wl['filtertop'] == posy), 'purpose'].iloc[0]
            gid3 = df_wl.loc[(df_wl['sum_pesticide'] == posx) & (df_wl['filtertop'] == posy), 'use'].iloc[0]

            annot.xycoords = line.axes.transData
            annot.xy = (posx, posy)
            text = f'{gid} ({gid2} - {gid3})'
            annot.set_text(text)
            annot.get_bbox_patch().set_alpha(1)

        def hover(event):
            vis = annot.get_visible()
            if event.inaxes in [ax]:
                for line in lines:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot(line, annot, ind['ind'][0])
                        annot.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot.set_visible(False)
                            fig.canvas.draw_idle()

        fig.canvas.mpl_connect('motion_notify_event', hover)
        def onclicker(event):
            for line in lines:
                if line.contains(event)[0]:
                        cont, ind = line.contains(event)
                        annot2 = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                                     bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                                     arrowprops=dict(arrowstyle="->"))
                        update_annot(line, annot2, ind['ind'][0])
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', onclicker)

        def clearAnn( event):
            if event.inaxes is None:  # event.inaxes returns None if you click outside of the plot area, i.e. in the grey part surrounding the axes
                for ann in ax.get_children():
                    if isinstance(ann, matplotlib.text.Annotation):
                        ann.set_visible(False)
                        event.canvas.draw()
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', clearAnn)
        print(f'Polygon: {self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}')

        fig_txt = f'Polygon: {self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}'
        print(fig_txt)

        plt.figtext(0.01, 0.01, fig_txt, fontsize=6, weight='bold')

        class NewTool(ToolBase):
                description = 'Dybdeplot af sum af pesticider. \n\nLegenden er interaktiv og du kan "hover" over et punkt og få boringens DGU-nummer, formål og brug. \n\nVed at indæstte punkter til QGIS (knap), kan du tegne en rektangel på plottet og de vælges herefter i QGIS.'

        class NewTool2(ToolBase):
            description = 'Indhente alle punkter fra plottet.'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                df_wl.to_csv('C:/Temp/pesticider.csv', index=False)

                path = 'file:///' + 'C:/Temp/pesticider.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                "latin0", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")
                #  "?delimiter=%s&crs=epsg:3857&xField=%s&yField=%s" % (",", "lon", "lat")

                mylayer = QgsVectorLayer(path, "Pesticider - sum", "delimitedtext")

                project.addMapLayer(mylayer)

        class NewTool3(ToolBase):
            description = 'Marker valgte punkter fra QGIS i plottet. (Kør efter "QGIS - Punkter fra plot")'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                try:
                    z = ax.collections
                    z.pop(0)
                    z.remove()

                except:
                    print("No such layer..")

                if len(project.mapLayersByName('Pesticider - sum')) == 0:
                    QMessageBox.warning(None, 'Meddelse', "Pesticider - sum skal først indhentes.")
                    pass
                else:
                    scatterlag = project.mapLayersByName('Pesticider - sum')[0]

                    valgtepunkter_features = scatterlag.selectedFeatures()

                    lyr = project.mapLayersByName('Pesticider - sum')[0]

                    cols = [f.name() for f in lyr.fields()]

                    df2 = pd.DataFrame(valgtepunkter_features, columns=cols)
                    plt.autoscale(False)
                    scat = plt.scatter(df2['sum_pesticide'], df2['filtertop'], s=120, facecolors='none', edgecolors='black')

                    plt.ion()
                    plt.draw()

        def line_select_callback(eclick, erelease):
            x1, y1 = eclick.xdata, eclick.ydata
            x2, y2 = erelease.xdata, erelease.ydata
            xdata = df_wl['sum_pesticide']
            ydata = df_wl['filtertop']

            mask = (xdata > min(x1, x2)) & (xdata < max(x1, x2)) & \
                   (ydata > min(y1, y2)) & (ydata < max(y1, y2))
            print(mask)

            listen = []
            for ind, m in enumerate(mask):
                if m == True:
                    pass
                else:
                    listen.append(ind)

            df3 = df_wl.drop(listen)
            markeringer = df3['dgu_intake'].tolist()
            markeringer2 = str(markeringer).replace('[', '').replace(']', '')
            print(markeringer)
            print(markeringer2)

            project = QgsProject.instance()

            scatterlag = project.mapLayersByName('Pesticider - sum')[0]

            expr = QgsExpression("\"dgu_intake\" IN ({})".format(str(markeringer2)))

            # it = scatterlag.getFeatures( QgsFeatureRequest( expr ) )
            ids = QgsFeatureRequest(QgsExpression(expr)).setFlags(QgsFeatureRequest.NoGeometry).setSubsetOfAttributes(
                [])
            selection = scatterlag.getFeatures(ids)  # = [i.id() for i in it]
            scatterlag.selectByIds([s.id() for s in selection])
            # scatterlag.setSelectedFeatures(selection)

        def toggle_selector(event):
            print(' Key pressed.')
            if event.key in ['Q', 'q'] and toggle_selector.RS.active:
                print(' RectangleSelector deactivated.')
                toggle_selector.RS.set_active(False)
            if event.key in ['A', 'a'] and not toggle_selector.RS.active:
                print(' RectangleSelector activated.')
                toggle_selector.RS.set_active(True)

        toggle_selector.RS = RectangleSelector(ax, line_select_callback,
                                               drawtype='box', useblit=True,
                                               button=[1], interactive=True)
        fig.canvas.mpl_connect('key_press_event', toggle_selector)

        tm = fig.canvas.manager.toolmanager
        tm.add_tool("Info (hover)", NewTool)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("Info (hover)"), "toolgroup")

        tm.add_tool("QGIS - Punkter fra plot", NewTool2)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Punkter fra plot"), "toolgroup")

        tm.add_tool("QGIS - Marker punkter på plot", NewTool3)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Marker punkter på plot"), "toolgroup")

        plt.show()


class DybdeplotNitratSulfat:
    """class handles the plotting of groundwater-chemical data from pcjupiterxl"""
    def __init__(self, x_min_val, y_min_val, x_max_val, y_max_val, data_type_val):
        """
        :param x_min_val: geometry extent xy-plane, minimum x-value
        :param y_min_val: geometry extent xy-plane, minimum y-value
        :param x_max_val: geometry extent xy-plane, maximum x-value
        :param y_max_val: geometry extent xy-plane, maximum x-value
        :param data_type_val: The chosen datatype for plotting
        """
        self.x_min = x_min_val
        self.y_min = y_min_val
        self.x_max = x_max_val
        self.y_max = y_max_val
        self.data_type = data_type_val[9:]

    def fetch_chemsamp(self, srid=25832):
        """

        :param srid:
        :return:
        """
        sql_wl = f'''
            WITH 
                compounds AS 
                    (
                        SELECT c.code::NUMERIC AS compoundno, c.longtext AS compound
                        FROM jupiter.code c 
                        WHERE c.codetype = 221	
                            AND longtext = '{self.data_type}'
                    ),
                chem_unit AS
                    (
                        SELECT c.code::NUMERIC AS unit, c.longtext AS unit_descr
                        FROM jupiter.code c 
                        WHERE c.codetype = 752
                    )
            SELECT DISTINCT ON (gcs.guid_intake) 
                gcs.guid_intake, 
                gcs.sampledate, 
                CASE
                    WHEN gca.attribute LIKE '<%' 
                        OR gca.attribute LIKE 'A%' 
                        OR gca.attribute LIKE 'o%' 
                        OR gca.attribute LIKE '0%' 
                        THEN 0::NUMERIC
                    WHEN gca.attribute LIKE 'B%' 
                        OR gca.attribute LIKE 'C%' 
                        OR gca.attribute LIKE 'D%' 
                        OR gca.attribute LIKE 'S%' 
                        OR gca.attribute LIKE '!%' 
                        OR gca.attribute LIKE '/%' 
                        OR gca.attribute LIKE '*%' 
                        OR gca.attribute LIKE '>%' 
                        THEN -99::NUMERIC
                    ELSE gca.amount
                    END AS amount_edit,
                u.unit_descr,
                concat(z.layer_num, '_', z.litho) AS fohm_layer,
                b.purpose,
                b.use,
				s.top as filtertop,
                concat(REPLACE(s.boreholeno, ' ', ''), '_', s.intakeno) AS dgu_intake,
                b.xutm32euref89,
                b.yutm32euref89
            FROM jupiter.grwchemanalysis gca
            INNER JOIN jupiter.grwchemsample gcs USING (sampleid)
            LEFT JOIN temp_simak.fohm_filter_lith_aquifer z USING (boreholeid) 
            INNER JOIN jupiter.borehole b USING (boreholeid)
            INNER JOIN jupiter.screen s USING (boreholeid)
            INNER JOIN compounds USING (compoundno)
            LEFT JOIN chem_unit u ON gca.unit = u.unit
            WHERE COALESCE(gcs.watertype, 0) NOT IN (1,3,4,5)
                AND COALESCE(upper(b.purpose), 'K') NOT IN ('L', 'VA')
                AND b.geom && ST_MakeEnvelope({self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}, {srid})
            ORDER BY gcs.guid_intake, gcs.sampledate DESC
            ;
        '''

        db = Database(database='JUPITER')
        df_wl = db.sql_to_df(sql_wl)

        return df_wl

    def plot_dybdeplot(self):
        """

        :return:
        """
        df_wl = self.fetch_chemsamp()
        df_wl['length'] = df_wl['fohm_layer'].str.len()
        df_wl['fohm_layer'] = df_wl['fohm_layer'].replace('_', 'Ukendt lag')
        df_wl.sort_values(['length', 'fohm_layer'], ascending=[True, True], inplace=True)
        df_wl_grp = df_wl.groupby(['fohm_layer'], sort=False)
        print(df_wl['fohm_layer'][0])
        pd.set_option('display.max_rows', 500)
        pd.set_option('display.max_columns', 500)
        pd.set_option('display.width', 1000)
        print(df_wl)
        from matplotlib import pyplot as plt, patches, rcParams, cm
        import numpy as np

        fig, ax = plt.subplots(figsize=[12,8])

        n = sum(df_wl_grp["unit_descr"].nunique())
        color = iter(cm.rainbow(np.linspace(0, 1, n)))

        for name, grp in df_wl_grp:
            c = next(color)
            line, = ax.plot(grp['amount_edit'].astype(np.float), grp['filtertop'].astype(np.float), marker='o',
                            linestyle='', label=name, c=c)


        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        lines = ax.get_lines()
        leg = ax.legend(fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5))
        lined = {}  # Will map legend lines to original lines.
        for legline, origline in zip(leg.get_lines(), lines):
            legline.set_picker(True)
            legline.set_pickradius(5)  # Enable picking on the legend line.
            lined[legline] = origline

        def on_pick(event):
            legline = event.artist
            origline = lined[legline]
            visible = not origline.get_visible()
            origline.set_visible(visible)
            legline.set_alpha(1.0 if visible else 0.2)
            fig.canvas.draw()

        if self.data_type == 'Nitrat':
            plt.axvline(x=50, color='r', linestyle='-')
        else:
            plt.axvline(x=250, color='r', linestyle='-')

        plt.title(f'{self.data_type} [mg/L]', fontweight='bold', fontsize=12)
        plt.grid(b=None, which='both', axis='both')
        ax.margins(y=0.05)

        ax.margins(x=0.05)
        plt.ylabel('Filtertop (mut)', fontweight='bold', fontsize=12)
        plt.gca().invert_yaxis()
        ax.tick_params(labelbottom=False, labeltop=True)
        ax.xaxis.set_ticks_position('top')
        ax.tick_params(labelbottom=False, labeltop=True)

        fig.canvas.mpl_connect('pick_event', on_pick)

        annot = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        def update_annot(line, annot, ind):
            posx, posy = [line.get_xdata()[ind], line.get_ydata()[ind]]

            gid = df_wl.loc[(df_wl['amount_edit'] == posx) & (df_wl['filtertop'] == posy), 'dgu_intake'].iloc[0]
            gid2 = df_wl.loc[(df_wl['amount_edit'] == posx) & (df_wl['filtertop'] == posy), 'purpose'].iloc[0]
            gid3 = df_wl.loc[(df_wl['amount_edit'] == posx) & (df_wl['filtertop'] == posy), 'use'].iloc[0]
            annot.xycoords = line.axes.transData
            annot.xy = (posx, posy)
            text = f'{gid} ({gid2} - {gid3})'
            annot.set_text(text)
            annot.get_bbox_patch().set_alpha(1)

        def hover(event):
            vis = annot.get_visible()
            if event.inaxes in [ax]:
                for line in lines:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot(line, annot, ind['ind'][0])
                        annot.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot.set_visible(False)
                            fig.canvas.draw_idle()

        fig.canvas.mpl_connect('motion_notify_event', hover)
        def onclicker(event):
            for line in lines:
                if line.contains(event)[0]:
                        cont, ind = line.contains(event)
                        annot2 = ax.annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                                     bbox=dict(boxstyle="round", facecolor="#FFFFFF"),
                                     arrowprops=dict(arrowstyle="->"))
                        update_annot(line, annot2, ind['ind'][0])
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', onclicker)

        def clearAnn( event):
            if event.inaxes is None:  # event.inaxes returns None if you click outside of the plot area, i.e. in the grey part surrounding the axes
                for ann in ax.get_children():
                    if isinstance(ann, matplotlib.text.Annotation):
                        ann.set_visible(False)
                        event.canvas.draw()
                        fig.canvas.draw()

        fig.canvas.mpl_connect('button_press_event', clearAnn)
        fig_txt = f'Polygon: {self.x_min}, {self.y_min}, {self.x_max}, {self.y_max}'
        print(fig_txt)

        plt.figtext(0.01, 0.01, fig_txt, fontsize=6, weight='bold')
        datatype = self.data_type
        class NewTool(ToolBase):
            if datatype == 'Nitrat':
                description = 'Dybdeplot af nitrat. \n\nLegenden er interaktiv og du kan "hover" over et punkt og få boringens DGU-nummer, formål og brug.\n\nVed at indæstte punkter til QGIS (knap), kan du tegne en rektangel på plottet og de vælges herefter i QGIS.'
            if datatype == 'Sulfat':
                description = 'Dybdeplot af sulfat. \n\nLegenden er interaktiv og du kan "hover" over et punkt og få boringens DGU-nummer, formål og brug.\n\nVed at indæstte punkter til QGIS (knap), kan du tegne en rektangel på plottet og de vælges herefter i QGIS.'

        class NewTool2(ToolBase):
            description = 'Indhente alle punkter fra plottet.'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                df_wl.to_csv('C:/Temp/nitratsulfat.csv', index=False)

                path = 'file:///' + 'C:/Temp/nitratsulfat.csv' + '?encoding=%s&delimiter=%s&xField=%s&yField=%s&crs=%s' % (
                    "latin0", ",", "xutm32euref89", "yutm32euref89", "epsg:25832")
                #  "?delimiter=%s&crs=epsg:3857&xField=%s&yField=%s" % (",", "lon", "lat")
                mylayer = QgsVectorLayer(path, "{}".format(datatype), "delimitedtext")

                project.addMapLayer(mylayer)

        class NewTool3(ToolBase):
            description = 'Marker valgte punkter fra QGIS i plottet. (Kør efter "QGIS - Punkter fra plot")'

            def trigger(self, *args, **kwargs):
                project = QgsProject.instance()

                try:
                    z = ax.collections
                    z.pop(0)
                    z.remove()

                except:
                    print("No such layer..")

                if len(project.mapLayersByName("{}".format(datatype))) == 0:
                    QMessageBox.warning(None, 'Meddelse', "{} skal først indhentes.".format(datatype))
                    pass
                else:
                    scatterlag = project.mapLayersByName("{}".format(datatype))[0]

                    valgtepunkter_features = scatterlag.selectedFeatures()

                    lyr = project.mapLayersByName("{}".format(datatype))[0]

                    cols = [f.name() for f in lyr.fields()]

                    df2 = pd.DataFrame(valgtepunkter_features, columns=cols)
                    plt.autoscale(False)
                    scat = plt.scatter(df2['amount_edit'], df2['filtertop'], s=120, facecolors='none', edgecolors='black')

                    plt.ion()
                    plt.draw()

        def line_select_callback(eclick, erelease):
            x1, y1 = eclick.xdata, eclick.ydata
            x2, y2 = erelease.xdata, erelease.ydata
            xdata = df_wl['amount_edit']
            ydata = df_wl['filtertop']

            mask = (xdata > min(x1, x2)) & (xdata < max(x1, x2)) & \
                   (ydata > min(y1, y2)) & (ydata < max(y1, y2))
            print(mask)

            listen = []
            for ind, m in enumerate(mask):
                if m == True:
                    pass
                else:
                    listen.append(ind)

            df3 = df_wl.drop(listen)
            markeringer = df3['dgu_intake'].tolist()
            markeringer2 = str(markeringer).replace('[', '').replace(']', '')
            print(markeringer)
            print(markeringer2)

            project = QgsProject.instance()

            scatterlag = project.mapLayersByName("{}".format(datatype))[0]

            expr = QgsExpression("\"dgu_intake\" IN ({})".format(str(markeringer2)))

            # it = scatterlag.getFeatures( QgsFeatureRequest( expr ) )
            ids = QgsFeatureRequest(QgsExpression(expr)).setFlags(QgsFeatureRequest.NoGeometry).setSubsetOfAttributes(
                [])
            selection = scatterlag.getFeatures(ids)  # = [i.id() for i in it]
            scatterlag.selectByIds([s.id() for s in selection])
            # scatterlag.setSelectedFeatures(selection)

        def toggle_selector(event):
            print(' Key pressed.')
            if event.key in ['Q', 'q'] and toggle_selector.RS.active:
                print(' RectangleSelector deactivated.')
                toggle_selector.RS.set_active(False)
            if event.key in ['A', 'a'] and not toggle_selector.RS.active:
                print(' RectangleSelector activated.')
                toggle_selector.RS.set_active(True)

        toggle_selector.RS = RectangleSelector(ax, line_select_callback,
                                               drawtype='box', useblit=True,
                                               button=[1], interactive=True)
        fig.canvas.mpl_connect('key_press_event', toggle_selector)

        tm = fig.canvas.manager.toolmanager
        tm.add_tool("Info (hover)", NewTool)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("Info (hover)"), "toolgroup")

        tm.add_tool("QGIS - Punkter fra plot", NewTool2)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Punkter fra plot"), "toolgroup")

        tm.add_tool("QGIS - Marker punkter på plot", NewTool3)
        fig.canvas.manager.toolbar.add_tool(tm.get_tool("QGIS - Marker punkter på plot"), "toolgroup")

        plt.show()

def plot_data(data_type, points):
    """
    function handles the plotting classes based on the data type (e.g. waterlevel, skytem, nitrat)
    :param points: list of points of extent [x_min, y_min, x_max, y_max]
    :param x_min: geometry extent xy-plane, minimum x-value
    :param y_min: geometry extent xy-plane, minimum y-value
    :param x_max: geometry extent xy-plane, maximum x-value
    :param y_max: geometry extent xy-plane, maximum x-value
    :param data_type: The chosen datatype for plotting
    """
    print(data_type)
    print(points)
    x_min, y_min = points[0]
    x_max, y_max = points[1]
    geophysics = ['skytem', 'tem', 'ttem', 'mep', 'schlumberger', 'paces',]
    chemsamps = ['Nitrat', 'Sulfat']
    dybdeplots = ['DybdeplotNitrat', 'DybdeplotSulfat']
  ##  martins = ['Pejlinger', 'DybdeplotPesticider', 'DybdeplotVandtype', 'SulfatKlorid', 'NitratSulfat', 'Chemtable']
    if data_type in geophysics:
        try:
            p = Geophysics(x_min, y_min, x_max, y_max, data_type)
            p.plot_geophysics()
        except Exception as e:
            print(e)
            print('Geophysics did not successfully complete!')
    elif data_type == 'waterlevel':
        try:
            p = Waterlevel(x_min, y_min, x_max, y_max, data_type)
            p.plot_waterlevel()
        except Exception as e:
            print(e)
            print('Waterlevel did not successfully complete!')
    elif data_type in chemsamps:
        try:
            p = Chemsamp(x_min, y_min, x_max, y_max, data_type)
            p.plot_chemsamp()
        except Exception as e:
            print(e)
            print('Chemsamp did not successfully complete!')
    elif data_type == 'Chemicaltable':
        try:
            p = Chemtable(x_min, y_min, x_max, y_max, data_type)
            p.plot_table()
        except Exception as e:
            print(e)
            print('Chemical Table did not successfully complete!')
    elif data_type == 'log':
        try:
            gl = GeophysicalLog(x_min, y_min, x_max, y_max, data_type)
            gl.plot_log()
        except Exception as e:
            print(e)
            print('Geophysical Log did not successfully complete!')
    elif data_type == 'lith':
        try:
            gl = LithologyLog(x_min, y_min, x_max, y_max, data_type)
            gl.plot_lithology()
        except Exception as e:
            print(e)
            print('Lithology logs did not successfully complete!')
    elif data_type in dybdeplots:
        try:
            mk = DybdeplotNitratSulfat(x_min, y_min, x_max, y_max, data_type)
            mk.plot_dybdeplot()
        except Exception as e:
            print(e)
            print('Dybdeplot did not successfully complete!')
    elif data_type == 'DybdeplotPesticider':
        try:
            mk = DybdeplotPesticiderSum(x_min, y_min, x_max, y_max, data_type)
            mk.plot_dybdeplot()
        except Exception as e:
            print(e)
            print('Dybdeplot did not successfully complete!')
    elif data_type == 'DybdeplotVandtype':
        try:
            mk = DybdePlotVandType(x_min, y_min, x_max, y_max, data_type)
            mk.plot_dybdeplot()
        except Exception as e:
            print(e)
            print('Dybdeplot did not successfully complete!')
    elif data_type == 'NitratSulfat':
        try:
            mk = ScatterSulfatNitrat(x_min, y_min, x_max, y_max, data_type)
            mk.plot_sulfatnitrat()
        except Exception as e:
            print(e)
            print('Nitrat/Sulfat did not successfully complete!')
    elif data_type == 'SulfatKlorid':
        try:
            mk = SulfatKlorid(x_min, y_min, x_max, y_max, data_type)
            mk.plot_sulfatklorid()
        except Exception as e:
            print(e)
            print('Sulfat/Klorid did not successfully complete!')
    elif data_type == 'VulnerabilityStatistics':
        try:
            mk = VulnerabilityStatistics(x_min, y_min, x_max, y_max, data_type)
            mk.plot_vulnstat()
            # df_vs,f,radio,check,diff=mk.plot_vulnstat2() #virker ikke lige nu, but maybe someday...
            # check.on_clicked(mk.func(diff))
            # radio.on_clicked(mk.tykfunc(f,diff,df_vs))
            # plt.ion()
            # input("Press Enter to continue...")
        except Exception as e:
            print(e)
            print('VulnerabilityStatistics did not successfully complete!')
    else:
        pass
    plt.show()

if __name__ == "__main__":
    """ This is executed when run directly from the command line, for test purpose"""
    # xy = [561545.6, 6181127.1, 561768.7, 6181240.9, 'skytem']
    # xy = [560026.0, 6180645.2, 560249.1, 6180759.0, 'Nitrat']
    # xy = [560026.0, 6180645.2, 560249.1, 6180759.0, 'waterlevel']
    # xy = [561725.3, 6178810.4, 562618.0, 6179265.5, 'tem']
    # xy = [529991.7, 6311316.1, 530103.3, 6311373.0, 'mep']
    # xy = [499002.3, 6298819.9, 499030.2, 6298834.1, 'ttem']
    # xy = [570174.8, 6219870.0, 570185.0, 6219875.2, 'paces']
    # xy = [520462.1, 6251261.5, 522187.6, 6253642.4, 'log']
    # xy = [577561.6, 6234528.7, 578050.5, 6234803.2, 'log']
    # xy = [678823.4, 6204590.2, 679174.2, 6204897.6, 'lith']
    # xy = [490659.4, 6287355.4, 491361.1, 6287970.2, 'lith']
    #xy = [491346.3, 6092271.9, 491478.0, 6092381.9, 'lith']
    xy = [491346.3, 6092271.9, 495478.0, 6112381.9, 'VulnerabilityStatistics']






    # geophysics = ['skytem', 'tem', 'ttem', 'mep', 'schlumberger', 'paces']
    # chemsamps = ['Nitrat', 'Sulfat']
    # 'waterlevel'
    plot_data(xy[4], xy[0], xy[1], xy[2], xy[3])
