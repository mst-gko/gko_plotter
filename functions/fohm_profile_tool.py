import re
import subprocess

import psycopg2 as pg
import pandas as pd
import configparser
from matplotlib import pyplot as plt, patches, rcParams
from scipy.interpolate import PchipInterpolator
import numpy as np


class Database:
    """Class handles all database related function"""

    def __init__(self, database_name, usr):
        """
        Initiates the classe Database
        :param database_name: string used to specify which database name on MST-Grundvand server to connect to
        :param usr: string used to specify the user, e.g. reader og writer
        """
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    @property
    def parse_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: dict containing db credentials
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.ini_database_name}']['userid']
        pw_read = config[f'{self.ini_database_name}']['password']
        host = config[f'{self.ini_database_name}']['host']
        port = config[f'{self.ini_database_name}']['port']
        dbname = config[f'{self.ini_database_name}']['databasename']

        credentials = dict()
        credentials['usr'] = usr_read
        credentials['pw'] = pw_read
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def connect_to_pgsql_db(self):
        """
        Connects to postgresql server database
        :return: postgresql psycopg2 connection and string database name
        """
        cred = self.parse_db_credentials
        pg_database_name = cred['dbname']
        pg_con = None
        try:
            pg_con = pg.connect(
                host=cred['host'],
                port=cred['port'],
                database=cred['dbname'],
                user=cred['usr'],
                password=cred['pw']
            )
        except Exception as e:
            print(e)
        else:
            del cred
            # print(f'Connected to database: {pg_database_name}')

        return pg_con, pg_database_name

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query string for the import
        :return: pandas dataframe containing output from sql query
        """
        conn, database = self.connect_to_pgsql_db()
        try:
            # print(f'Fetching table from {database} to dataframe...')
            df_pgsql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {database}')
        else:
            # print(f'Table loaded into dataframe from database: {database}')
            pass

        return df_pgsql

    def execute_pg_sql(self, sql):
        """
        connect to postgresql database and execute sql
        :param sql: string with sql query
        :return: None
        """
        cred = self.parse_db_credentials
        pgdatabase = cred['dbname']
        try:
            with pg.connect(
                    host=cred['host'],
                    port=cred['port'],
                    database=cred['dbname'],
                    user=cred['usr'],
                    password=cred['pw']
            ) as con:
                with con.cursor() as cur:
                    cur.execute(sql)
                    con.commit()
        except Exception as e:
            print(f'Unable to execute sql: {pgdatabase}:')
            print(e)
        else:
            del cred


class CrossSection:
    """
        A simple run-function to minimize the code within qgis script
        :param line_points: list of tuples, the vertices of the profile line in srid 25832
                eg. [(513425, 6296131), (529416, 6310901)]
        :param number_of_points: int, number of profile nodes along the profile line
        :param display_fig_descr: bool, if true: add figure description to the plot
        :param smooth_line: bool, if true: smoothing using pchip interpolation between the profile points
                if false: matplotlibs standard linear interpolation between the profile points
        :param display_profile_points: bool, if true: add lines displaying the position of the profile points
        :param database_name: string, name of the database containing the fohm layers
        :param schema_name: string, schema name with in the database containing the fohm layers
    """

    def __init__(
            self,
            line_points,
            number_of_points=100,
            smooth_line=False,
            display_fig_descr=True,
            display_profile_points=True,
            database_name='fohm',
            schema_name='fohm_layer'
    ):
        self.database_name = database_name
        self.db = Database(database_name=self.database_name, usr='reader')
        self.schema_name = schema_name
        self.points = line_points
        self.line_wkt = self.points_to_line_wkt()
        self.srid = 25832
        self.n = number_of_points
        self.smooth_lines = smooth_line
        self.display_fig_descr = display_fig_descr
        self.display_profile_points = display_profile_points
        self.df_table_name = self.fetch_table_names()
        self.sql = self.fetch_cross_section_sql()
        self.fig_txt = ''

    @staticmethod
    def interpolate_layers(x_data, df, n_interp=1000):
        """
        interpolate all columns of a pandas dataframe (y_data) and x_data using pchip interpolation
        :param x_data: numpy array, data (1st axis) used in the interpolation
        :param df: pandas dataframe, each row is data (2nd axis) used in the interpolation
        :param n_interp: int, number of nodes used in the interpolation
        :return: numpy array (1st axis) and pandas dataframe (2nd axis)
        """
        x_interp = None
        df_interp = pd.DataFrame()

        for (col_name, col_val) in df.iteritems():
            y_f = PchipInterpolator(x_data, col_val)  # , kind='cubic')
            x_interp = np.linspace(x_data.min(), x_data.max(), n_interp)
            y_interp = y_f(x_interp)
            df_interp[col_name] = y_interp

        return x_interp, df_interp

    def points_to_line_wkt(self):
        """
            Converts a list of tuple containing x,y coords to WKT linestring.
            :return: string, WKT linestring
        """
        points_txt = ''
        for i, (x, y) in enumerate(self.points):
            if i == 0:
                points_txt += f'{x} {y}'
            else:
                points_txt += f', {x} {y}'

        line_wkt = f'LINESTRING({points_txt})'

        return line_wkt

    def fetch_table_names(self):
        """
            Returns the table_names within the fohm database/schema
            :return: string, sql
        """
        sql_table_name = f'''
            SELECT table_name AS table_name
            FROM information_schema.tables
            WHERE table_schema='{self.schema_name}'
                AND table_type='BASE TABLE'
            ORDER BY table_name;
        '''
        df_table_name = self.db.import_pgsql_to_df(sql_table_name)

        return df_table_name

    def fetch_cross_section_sql(self):
        """
        return sql for querying the fohm raster layers as point along the line
        :return: string, sql
        """
        n_inv = 1 / self.n

        sql_join = ''
        sql_ele = ''
        for i, (index, row) in enumerate(self.df_table_name.iterrows()):
            table_name = row['table_name']
            layer_name = re.search("(\d+.[A-Za-z0-9]+.[A-Za-z0-9]+)", table_name)[0]
            sql_join += f'\nINNER JOIN fohm_layer."{table_name}" r{i} ON st_intersects(r{i}.rast, b.geom)'
            sql_ele += f'\n(ST_ValueCount(ST_Clip(r{i}.rast,b.geom))).value AS "{layer_name}",'

        sql = f'''
            WITH
                lines AS 
                    (
                        SELECT
                            ST_GeomFromText('{self.line_wkt}', {self.srid}) AS temp_line
                    ),
                points_from_line AS 
                    (
                        SELECT 
                            (ST_DumpPoints(ST_LineInterpolatePoints(temp_line, {n_inv}))).geom AS geom
                        FROM lines 
                    ),
                profile_dist AS 
                    (
                        SELECT 
                            row_number() OVER () AS row_num,
                            geom,
                            round((COALESCE(ST_Distance(geom, lag(geom,1) OVER ()), 0))::NUMERIC, 0) AS dist
                        FROM points_from_line
                    ),
                profile_dist_cumsum AS 
                    (
                        SELECT 
                            geom,
                            sum(dist) OVER (ORDER BY row_num) AS distance
                        FROM profile_dist 
                    )         
            SELECT
                {sql_ele}
                distance
            FROM profile_dist_cumsum b                
            {sql_join}
            ;
        '''

        return sql

    def plot_layers(self):
        """
            Plot the fohm layers using matplotlib's fill_between function
            The layers are only plotted if the layer if different than the layer above (previous)
            :return: None
        """
        sql = self.fetch_cross_section_sql()
        df = self.db.import_pgsql_to_df(sql)

        # put profile distance in separate var and del from dataframe
        x_dist = df['distance'].to_numpy()
        x_dist_orig = x_dist
        df.pop('distance')

        # set vars for deactivating layers on plot legend
        fill_plots = []

        # fixate figure dpi, due to large screen resolution
        # rcParams['figure.dpi'] = 150

        # create figure and plot
        fig, ax = plt.subplots()

        # define colors of fohm layers
        colors = {
            '0000_terrain_bathymetri': 'black',
            '0050_antropocaen_bund': 'lavender',
            '0100_postglacial_toerv': 'peru',
            '0200_kvartaer_sand': 'red',
            '0300_kvartaer_ler': 'sienna',
            '0400_kvartaer_sand': 'red',
            '1100_kvartaer_ler': 'sienna',
            '1200_kvartaer_sand': 'red',
            '1300_kvartaer_ler': 'sienna',
            '1400_kvartaer_sand': 'red',
            '1500_kvartaer_ler': 'sienna',
            '2100_kvartaer_sand': 'red',
            '2200_kvartaer_ler': 'sienna',
            '2300_kvartaer_sand': 'red',
            '2400_kvartaer_ler': 'sienna',
            '5100_maadegruppen_gram': 'darkorange',
            '5200_oevre_odderup': 'deepskyblue',
            '5300_oevre_arnum': 'darkorange',
            '5400_nedre_odderup': 'deepskyblue',
            '5500_nedre_arnum': 'darkorange',
            '5600_bastrup_bads6': 'deepskyblue',
            '5700_klintinghoved_krl6': 'darkorange',
            '5800_bastrup_bads5': 'deepskyblue',
            '5900_klintinghoved_krl5': 'darkorange',
            '6000_bastrup_bads4': 'deepskyblue',
            '6100_klintinghoved_krl4': 'darkorange',
            '6200_bastrup_bads3': 'deepskyblue',
            '6300_klintinghoved_krl3': 'darkorange',
            '6400_bastrup_bads2': 'deepskyblue',
            '6500_klintinghoved_krl2': 'darkorange',
            '6600_bastrup_bads1': 'deepskyblue',
            '6700_klintinghoved_krl1': 'darkorange',
            '6800_billund_bds6': 'deepskyblue',
            '6900_vejle_fjord': 'darkorange',
            '7000_billund_bds4': 'deepskyblue',
            '7100_vejle_fjord': 'darkorange',
            '7200_billund_bds3': 'deepskyblue',
            '7300_vejle_fjord': 'darkorange',
            '7400_billund_bds2': 'deepskyblue',
            '7500_vejle_fjord': 'darkorange',
            '7600_billund_bds1': 'deepskyblue',
            '7700_vejle_fjord': 'darkorange',
            '7800_billund_bds0': 'deepskyblue',
            '8000_palaeogen_ler': 'navy',
            '8500_danien_kalk': 'darkgreen',
            '9000_skrivekridt_bund': 'lawngreen',
            '9500_stensalt_top': 'black',
            '9499_dummy_layer': 'lightgrey'
        }

        # smooth lines if selected
        if self.smooth_lines:
            x_dist, df = self.interpolate_layers(x_dist, df)

        # loop through all layers
        column_names = df.columns
        for i, column_name in enumerate(column_names):
            bot = df[column_names[i]]  # layer bot as bot
            top = df[column_names[i - 1]]  # previous layer bot as top
            layer_id = re.search('\d+', column_name)[0]  # layer number e.g. 0000_terrain => 0000

            # plot layer if not a zero-thickness layer
            if not bot.equals(top):
                alpha = 1
                if layer_id == '9500':  # layer 9500 is a top-layer not bottom-layer
                    ax.plot(x_dist, bot, zorder=3, color='deeppink', label=column_name, linewidth=1)
                    column_name = '9499_dummy_layer'
                    alpha = 0.8

                if layer_id != '0000':  # discard terrain plot
                    pl = ax.fill_between(
                        x_dist, top, bot,
                        label=column_name, zorder=2, color=colors[column_name],
                        alpha=alpha, linewidth=0.2, edgecolor='black'
                    )
                    fill_plots.append(pl)

        # dislay points where fohm layers are extracted
        bot_min = df.min().min()
        top_max = df.max().max()
        if self.display_profile_points:
            plts = []
            for k, x in enumerate(x_dist_orig):
                tmp_y = [bot_min - 10, top_max + 10]
                tmp_x = [x, x]

                pl, = ax.plot(
                    tmp_x, tmp_y,
                    label='Profile points', color='gray',
                    linestyle='--', zorder=1, alpha=0.5, linewidth=1
                )
                pl.set_dashes([2, 2, 2, 2])
                plts.append(pl)
            plt.setp(plts[1:], label="_")

        # sort legend labels and handles by labels
        handles, labels = ax.get_legend_handles_labels()
        labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))

        # set legend on plot and place it outside plot
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        leg = ax.legend(handles, labels, fancybox=True, shadow=True, loc='center left', bbox_to_anchor=(1, 0.5))

        leg_to_fill = {}  # map legend objects to fill plots.
        for leg_obj, fill_plot in zip(leg.findobj(patches.Rectangle), fill_plots):
            leg_obj.set_picker(True)  # Enable picking on the legend object.
            leg_to_fill[leg_obj] = fill_plot

        def on_pick(event):
            leg_obj_orig = event.artist
            fill_plot_orig = leg_to_fill[leg_obj_orig]
            visible = not fill_plot_orig.get_visible()
            fill_plot_orig.set_visible(visible)
            leg_obj_orig.set_alpha(1.0 if visible else 0.2)
            leg_obj_orig.set_color(fill_plot_orig.get_facecolor())  # needed if run directly
            fig.canvas.draw()

        # set grid on plot
        ax.grid('both')

        # add title and labels to figure
        plt.title(f'FOHM profile')
        plt.xlabel('Profile distance [m]')
        plt.ylabel('Elevation, DVR90 [m]')

        # add figure description if activated
        if self.smooth_lines:
            smooth_text = 'pchip'
        else:
            smooth_text = 'None'

        if self.display_fig_descr:
            self.fig_txt += f'PROFILE-LINE: {self.line_wkt}'
            self.fig_txt += f'\nSRID: {self.srid}'
            self.fig_txt += f'\nNUMBER OF POINTS ALONG PROFILE: {self.n}'
            self.fig_txt += f'\nDISTANCE BETWEEN PROFILE POINTS: {round(x_dist_orig[1], 2)} m'
            self.fig_txt += f'\nLAYER SMOOTH: {smooth_text}'
            print(f'\n{self.fig_txt}')
            plt.figtext(0.01, 0.9, self.fig_txt, fontsize=6, weight='bold')

        fig.canvas.mpl_connect('pick_event', on_pick)
        plt.show()


def plot_data(line_points, number_of_points=100, display_fig_descr=True, smooth_line=True,
              display_profile_points=True):
    """
    A simple run-function to minimize the code within qgis script
    :param line_points: list of tuples, the vertices of the profile line in srid 25832
            eg. [(513425, 6296131), (529416, 6310901)]
    :param number_of_points: int, number of profile nodes along the profile line
    :param display_fig_descr: bool, if true: add figure description to the plot
    :param smooth_line: bool, if true: smoothing using pchip interpolation between the profile points
            if false: matplotlibs standard piecewise linear interpolation between the profile points
    :param display_profile_points: bool, if true: add lines displaying the position of the profile points
    """
    point1 = line_points[0]
    point2 = line_points[1]
    if point1 != point2:
        CrossSection(
            line_points=line_points,
            number_of_points=number_of_points,
            display_fig_descr=display_fig_descr,
            smooth_line=smooth_line,
            display_profile_points=display_profile_points
        ).plot_layers()
    else:
        print('ERROR! Profile start-point is the same as end-point')


if __name__ == "__main__":
    """ This is executed when run directly from the command line, for test purpose"""
    test_line_points = [(508516, 6114510), (508516, 6119966), (514422, 6119966)]
    plot_data(line_points=test_line_points)
