import configparser
import os.path

from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication, Qt, QLocale
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction
from PyQt5.QtWidgets import QLabel

from .resources import *  # needed to include icons
from .script_maptool import ScriptMaptool, ScriptLinetool
from .gko_plotter_dialog import GkoPlotterAbout

__author__ = 'Simon Makwarth <simak@mst.dk>'
__maintainer__ = 'Simon Makwarth <simak@mst.dk>'


class GkoPlotter:
    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface

        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)

        # initialize locale
        # attempt to fix error when installing plugin
        # https://github.com/nextgis/quickmapservices/commit/b6ddcd83f0f91872df7662008ff1852e7e862aac
        try:
            locale = QSettings().value('locale/userLocale')[0:2]
        except:
            locale = QSettings().value('locale/userLocale', QLocale().name())[0:2]

        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'GKO plotter_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = QCoreApplication.translate('GKO plotter', u'&GKO plotter')
        self.toolbar = self.iface.addToolBar(u'GKO plotter')
        self.toolbar.setObjectName(u'GKO plotter')

        self.pluginIsActive = False
        self.dockwidget = None

    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None
    ):

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        # Check if plugin was started the first time in current QGIS session
        #     # Must be set in initGui() to survive plugin reloads
        self.first_start = None

        return action

    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('&GKO plotter', message)

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&GKO plotter'),
                action
            )
            self.iface.removeToolBarIcon(action)

    def initGui(self):
        icon_path = ':/plugins/gko_plotter/icons/info.png'
        message = u'About'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            add_to_toolbar=False,
            callback=self.run_help,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/skytem.png'
        message = u'Plot SkyTEM'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_skytem_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/ttem.png'
        message = u'Plot tTEM'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_ttem_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/tem.png'
        message = u'Plot TEM'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_tem_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/mep.png'
        message = u'Plot MEP'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_mep_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/paces.png'
        message = u'Plot PACES'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_paces_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/log.png'
        message = u'Plot Geophysical Logs'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_log_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/lith.png'
        message = u'Plot Lithology Logs'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_lith_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/fohm_profil.png'
        message = u'Plot FOHM profil'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_fohm_profile_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/waterlevel.png'
        message = u'Plot Waterlevel'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_waterlevel_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/nitrat.png'
        message = u'Plot Nitrate'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_nitrate_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/sulfat.png'
        message = u'Plot Sulphate'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_sulphate_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/dnitrat.png'
        message = u'Plot Dybdeplot - Nitrat'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_dnitrat_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/dsulfat.png'
        message = u'Plot Dybdeplot - Sulfat'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_dsulfat_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/watertype.png'
        message = u'Plot Dybdeplot - Vandtype'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_dvandtype_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/pest.png'
        message = u'Plot Dybdeplot - Pesticider'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_dpesticer_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/no3so4.png'
        message = u'Plot Nitrat/Sulfat'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_nitratsulfat_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/so4cl.png'
        message = u'Plot Sulfat/Klorid'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_sulfatklorid_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/ct.png'
        message = u'Plot Chemical Table'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_chemicaltable_plotter,
            parent=self.iface.mainWindow()
        )

        icon_path = ':/plugins/gko_plotter/icons/FOHM-JUPITER.png'
        message = u'Plot FOHM-JUPITER comparison'
        button_descr = QCoreApplication.translate('Gkoplotter', message)
        self.add_action(
            icon_path,
            text=button_descr,
            callback=self.run_vulnstat_plotter,
            parent=self.iface.mainWindow()
        )









    def run_help(self):
        d = GkoPlotterAbout(self.iface)
        d.exec_()

    def run_skytem_plotter(self):
        msg = 'skytem'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_tem_plotter(self):
        msg = 'tem'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_ttem_plotter(self):
        msg = 'ttem'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_mep_plotter(self):
        msg = 'mep'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_paces_plotter(self):
        msg = 'paces'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_log_plotter(self):
        msg = 'log'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_lith_plotter(self):
        msg = 'lith'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_waterlevel_plotter(self):
        msg = 'waterlevel'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_nitrate_plotter(self):
        msg = 'Nitrat'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_sulphate_plotter(self):
        msg = 'Sulfat'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_Potentialekort_plotter(self):
        msg = 'Potentialekort'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_dpesticer_plotter(self):
        msg = 'DybdeplotPesticider'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_dnitrat_plotter(self):
        msg = 'DybdeplotNitrat'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_chemicaltable_plotter(self):
        msg = 'Chemicaltable'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)
        
    def run_vulnstat_plotter(self):
        msg = 'VulnerabilityStatistics'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)
        
    def run_dsulfat_plotter(self):
        msg = 'DybdeplotSulfat'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_dvandtype_plotter(self):
        msg = 'DybdeplotVandtype'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_sulfatklorid_plotter(self):
        msg = 'SulfatKlorid'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_nitratsulfat_plotter(self):
        msg = 'NitratSulfat'
        self.rect_tool = ScriptMaptool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)

    def run_fohm_profile_plotter(self):
        msg = 'fohm'
        self.rect_tool = ScriptLinetool(self.iface, tag=msg)
        self.iface.mapCanvas().setMapTool(self.rect_tool)
