# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load GkoPlottingTool class from file GkoPlottingTool.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .gko_plotter import GkoPlotter
    return GkoPlotter(iface)
